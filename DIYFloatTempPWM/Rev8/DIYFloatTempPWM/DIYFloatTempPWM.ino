/* DIY Float Temp PWM board 8th Order Ai Speak
 *   (AKA The Ryan)
 *
 * Copyright 2015 Scott Tomko, Greg Tomko, Linda Close
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * (GNU General Public License for more details.

 * You should have received a copy of the GNU Lesser General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * Contact:  Staff@ReefAi.com
 *
 */
 
#include <EEPROMex.h>  //For Calibration Data and ID

//Variables for testing

//Variable needed by all Ai Arduino's:
String InputString = "";
boolean StringComplete = false;
boolean Pause = false;
int Comand = 0;
int Comand1 = 0;
unsigned long WatchDogTime = 0;
int WatchDogEnable = 0;
unsigned long TimeNow = 0;
unsigned long StartTime = 0;
unsigned long LastSend = 0;
unsigned long LastMeasurement = 0;
boolean SendData = false;
boolean SendQue = false;
int SendDelay = 0;
char ID[] = "        ";
int ActuatorValInt = 0;   //Processed String Data in 3 data types:
float ActuatorValFloat = 0;
String ActuatorValString = "";
char ActuatorValCharArray[] = "        ";
char LEDBID[] = "        ";
int LEDB = 0;


//Analog and Digital Reading Section
const int numReadings = 30;  //number of analog readings in array
int index = 0;        // the index of the current reading
//Temperature Inputs
int DIYTemp1[numReadings];  // the readings from the analog input as an array
float DIYTemp1Total = 0;    // the running total
float DIYTemp1Avg = 0;    //The average
float CalDIYTempf1 = 0;  //Calibrated
float CalDIYTempc1 = 0;  //Calibrated
char DIYTempf1ID[] = "        ";
char DIYTempc1ID[] = "        ";

int DIYTemp2[numReadings];  // the readings from the analog input as an array
float DIYTemp2Total = 0;    // the running total
float DIYTemp2Avg = 0;    //The average
float CalDIYTempf2 = 0;  //Calibrated
float CalDIYTempc2 = 0;  //Calibrated
char DIYTempf2ID[] = "        ";
char DIYTempc2ID[] = "        ";

int DIYTemp3[numReadings];  // the readings from the analog input as an array
float DIYTemp3Total = 0;    // the running total
float DIYTemp3Avg = 0;    //The average
float CalDIYTempf3 = 0;  //Calibrated
float CalDIYTempc3 = 0;  //Calibrated
char DIYTempf3ID[] = "        ";
char DIYTempc3ID[] = "        ";

int DIYTemp4[numReadings];  // the readings from the analog input as an array
float DIYTemp4Total = 0;    // the running total
float DIYTemp4Avg = 0;    //The average
float CalDIYTempf4 = 0;  //Calibrated
float CalDIYTempc4 = 0;  //Calibrated
char DIYTempf4ID[] = "        ";
char DIYTempc4ID[] = "        ";

int DIYTemp5[numReadings];  // the readings from the analog input as an array
float DIYTemp5Total = 0;    // the running total
float DIYTemp5Avg = 0;    //The average
float CalDIYTempf5 = 0;  //Calibrated
float CalDIYTempc5 = 0;  //Calibrated
char DIYTempf5ID[] = "        ";
char DIYTempc5ID[] = "        ";

int DIYTemp6[numReadings];  // the readings from the analog input as an array
float DIYTemp6Total = 0;    // the running total
float DIYTemp6Avg = 0;    //The average
float CalDIYTempf6 = 0;  //Calibrated
float CalDIYTempc6 = 0;  //Calibrated
char DIYTempf6ID[] = "        ";
char DIYTempc6ID[] = "        ";

int DIYTemp7[numReadings];  // the readings from the analog input as an array
float DIYTemp7Total = 0;    // the running total
float DIYTemp7Avg = 0;    //The average
float CalDIYTempf7 = 0;  //Calibrated
float CalDIYTempc7 = 0;  //Calibrated
char DIYTempf7ID[] = "        ";
char DIYTempc7ID[] = "        ";

int DIYTemp8[numReadings];  // the readings from the analog input as an array
float DIYTemp8Total = 0;    // the running total
float DIYTemp8Avg = 0;    //The average
float CalDIYTempf8 = 0;  //Calibrated
float CalDIYTempc8 = 0;  //Calibrated
char DIYTempf8ID[] = "        ";
char DIYTempc8ID[] = "        ";

//PWM outputs
char PWM1ID[] = "        ";
int PWM1 = 0;
int PWM1FS = 0;   //Fail Safe

char PWM2ID[] = "        ";
int PWM2 = 0;
int PWM2FS = 0;

char PWM3ID[] = "        ";
int PWM3 = 0;
int PWM3FS = 0;

char PWM4ID[] = "        ";
int PWM4 = 0;
int PWM4FS = 0;

char PWM5ID[] = "        ";
int PWM5 = 0;
int PWM5FS = 0;

char PWM6ID[] = "        ";
int PWM6 = 0;
int PWM6FS = 0;

//Float Switches
char FloatSwitch1ID[] = "        ";
int FloatSwitch1 = 0;

char FloatSwitch2ID[] = "        ";
int FloatSwitch2 = 0;

char FloatSwitch3ID[] = "        ";
int FloatSwitch3 = 0;

char FloatSwitch4ID[] = "        ";
int FloatSwitch4 = 0;

char FloatSwitch5ID[] = "        ";
int FloatSwitch5 = 0;

char FloatSwitch6ID[] = "        ";
int FloatSwitch6 = 0;

//calibration variables
float DIYTemp1CalM = 0;
float DIYTemp1CalB = 0;
float DIYTemp2CalM = 0;
float DIYTemp2CalB = 0;
float DIYTemp3CalM = 0;
float DIYTemp3CalB = 0;
float DIYTemp4CalM = 0;
float DIYTemp4CalB = 0;
float DIYTemp5CalM = 0;
float DIYTemp5CalB = 0;
float DIYTemp6CalM = 0;
float DIYTemp6CalB = 0;
float DIYTemp7CalM = 0;
float DIYTemp7CalB = 0;
float DIYTemp8CalM = 0;
float DIYTemp8CalB = 0;


const int PWM1Pin = 3;
const int PWM2Pin = 5;
const int PWM3Pin = 6;
const int PWM4Pin = 9;
const int PWM5Pin = 10;
const int PWM6Pin = 11;

const int DIYTemp1Pin = 20;
const int DIYTemp2Pin = 21;
const int DIYTemp3Pin = 14;
const int DIYTemp4Pin = 19;
const int DIYTemp5Pin = 18;
const int DIYTemp6Pin = 17;
const int DIYTemp7Pin = 16;
const int DIYTemp8Pin = 15;

const int Float1Pin = 4;
const int Float2Pin = 2;
const int Float3Pin = 7;
const int Float4Pin = 8;
const int Float5Pin = 13;
const int Float6Pin = 12;


void setup() {
  Serial.begin(57600);
    // reserve 200 bytes for the inputString:
  InputString.reserve(200);
 
 // initialize all the analog readings to 0:
  for (int thisReading = 0; thisReading < numReadings; thisReading++) {
    DIYTemp1[thisReading] = 0;  
    DIYTemp2[thisReading] = 0;
    DIYTemp3[thisReading] = 0;  
    DIYTemp4[thisReading] = 0;
    DIYTemp5[thisReading] = 0;  
    DIYTemp6[thisReading] = 0;
    DIYTemp7[thisReading] = 0;  
    DIYTemp8[thisReading] = 0;
  }
 
 //Tell the pins to be inputs or outputs
  pinMode(DIYTemp1Pin, INPUT);
  pinMode(DIYTemp1Pin, INPUT);
  pinMode(DIYTemp1Pin, INPUT);
  pinMode(DIYTemp1Pin, INPUT);   
  pinMode(DIYTemp1Pin, INPUT);
  pinMode(DIYTemp1Pin, INPUT);
  pinMode(DIYTemp1Pin, INPUT);
  pinMode(DIYTemp1Pin, INPUT);   
  pinMode(Float1Pin, INPUT_PULLUP);
  pinMode(Float2Pin, INPUT_PULLUP);
  pinMode(Float3Pin, INPUT_PULLUP);
  pinMode(Float4Pin, INPUT_PULLUP);
  pinMode(Float5Pin, INPUT_PULLUP);
  pinMode(Float6Pin, INPUT_PULLUP);
  pinMode(PWM1Pin, OUTPUT);
  pinMode(PWM2Pin, OUTPUT);
  pinMode(PWM3Pin, OUTPUT);
  pinMode(PWM4Pin, OUTPUT);
  pinMode(PWM5Pin, OUTPUT);
  pinMode(PWM6Pin, OUTPUT);
  
  
  WatchDogTime = millis();  //Delay the watchdog
  
  //Read From EEPROM
  EEPROM.readBlock<char>(0, ID, 8);    //Read In Element ID's from EEPROM
  EEPROM.readBlock<char>(8, DIYTempf1ID, 8); 
  EEPROM.readBlock<char>(24, DIYTempc1ID, 8);
  EEPROM.readBlock<char>(40, DIYTempf2ID, 8);
  EEPROM.readBlock<char>(56, DIYTempc2ID, 8);
  EEPROM.readBlock<char>(72, DIYTempf3ID, 8);
  EEPROM.readBlock<char>(88, DIYTempc3ID, 8);
  EEPROM.readBlock<char>(104, DIYTempf4ID, 8);
  EEPROM.readBlock<char>(120, DIYTempc4ID, 8);
  EEPROM.readBlock<char>(136, DIYTempf5ID, 8);
  EEPROM.readBlock<char>(152, DIYTempc5ID, 8);
  EEPROM.readBlock<char>(168, DIYTempf6ID, 8);
  EEPROM.readBlock<char>(184, DIYTempc6ID, 8);
  EEPROM.readBlock<char>(200, DIYTempf7ID, 8);
  EEPROM.readBlock<char>(216, DIYTempc7ID, 8);
  EEPROM.readBlock<char>(232, DIYTempf8ID, 8);
  EEPROM.readBlock<char>(248, DIYTempc8ID, 8);
  
  EEPROM.readBlock<char>(264, FloatSwitch1ID, 8);
  EEPROM.readBlock<char>(272, FloatSwitch2ID, 8);
  EEPROM.readBlock<char>(280, FloatSwitch3ID, 8);
  EEPROM.readBlock<char>(288, FloatSwitch4ID, 8);
  EEPROM.readBlock<char>(296, FloatSwitch5ID, 8);
  EEPROM.readBlock<char>(304, FloatSwitch6ID, 8);
  
  EEPROM.readBlock<char>(312, PWM1ID, 8);
  EEPROM.readBlock<char>(320, PWM2ID, 8);
  EEPROM.readBlock<char>(328, PWM3ID, 8);
  EEPROM.readBlock<char>(336, PWM4ID, 8);
  EEPROM.readBlock<char>(344, PWM5ID, 8);
  EEPROM.readBlock<char>(352, PWM6ID, 8);
  
  WatchDogEnable = EEPROM.readInt(360);  //Watchdog Enable/Diable
  
  PWM1FS = EEPROM.readInt(362);
  PWM2FS = EEPROM.readInt(364);
  PWM3FS = EEPROM.readInt(366);
  PWM4FS = EEPROM.readInt(368);
  PWM5FS = EEPROM.readInt(370);
  PWM6FS = EEPROM.readInt(372);
 
  DIYTemp1CalM = EEPROM.readFloat(16);
  DIYTemp1CalB = EEPROM.readFloat(20);
  DIYTemp2CalM = EEPROM.readFloat(48);
  DIYTemp2CalB = EEPROM.readFloat(52);
  DIYTemp3CalM = EEPROM.readFloat(80);
  DIYTemp3CalB = EEPROM.readFloat(84);
  DIYTemp4CalM = EEPROM.readFloat(112);
  DIYTemp4CalB = EEPROM.readFloat(116);
  DIYTemp5CalM = EEPROM.readFloat(114);
  DIYTemp5CalB = EEPROM.readFloat(148);
  DIYTemp6CalM = EEPROM.readFloat(176);
  DIYTemp6CalB = EEPROM.readFloat(180);
  DIYTemp7CalM = EEPROM.readFloat(208);
  DIYTemp7CalB = EEPROM.readFloat(212);
  DIYTemp8CalM = EEPROM.readFloat(240);
  DIYTemp8CalB = EEPROM.readFloat(244);

//Assign a group ID of 00000000 if it doesn't have one
      char a = EEPROM.read(0);
      //Serial.println(a);
      if (a != 'g') {
        EEPROM.updateBlock<char>(0,"00000000", 8);
    //    Serial.println("writing 0's to EEPROM");
        EEPROM.readBlock<char>(0, ID, 8);
        Serial.println(ID);
       }
//Ask for Actuator Data at Startup
  Serial.print(ID);
  Serial.print("/");
  Serial.println("SendActData");
  }
 
 
void loop() {
    
TimeNow = millis();  //Check the time
  if ((StartTime + 60000 > TimeNow) && (SendQue)) { //do this for 1 minute 
    if (Pause) {
      SendData = false;
      Pause = false;
    }
    else {
      SendData = true;
    }
  }
  else {  //  Stop Sending Data
    SendData = false;
    SendQue = false;
    StartTime = 0;
    SendDelay = 0;
    LastSend = 0;
  }
  
  if (SendData) {  //Send Data at the specified Rate
    if (TimeNow > LastSend + SendDelay) {
          //Send Back Sensor Data to Ai
      Serial.print(ID);  //Group ID
      Serial.print("/");
      Serial.print(DIYTempf1ID);  //Temp in Farenheight
      Serial.print(":");
      Serial.print(CalDIYTempf1);
      Serial.print(",");
      Serial.print(DIYTempc1ID);  //Temp in Celcius
      Serial.print(":");
      Serial.print(CalDIYTempc1);
      Serial.print(",");
      Serial.print(DIYTempf2ID);  //Temp in Farenheight
      Serial.print(":");
      Serial.print(CalDIYTempf2);
      Serial.print(",");
      Serial.print(DIYTempc2ID);  //Temp in Celcius
      Serial.print(":");
      Serial.print(CalDIYTempc2);
      Serial.print(",");
      Serial.print(DIYTempf3ID);  //Temp in Farenheight
      Serial.print(":");
      Serial.print(CalDIYTempf3);
      Serial.print(",");
      Serial.print(DIYTempc3ID);  //Temp in Celcius
      Serial.print(":");
      Serial.print(CalDIYTempc3);
      Serial.print(",");
      Serial.print(DIYTempf4ID);  //Temp in Farenheight
      Serial.print(":");
      Serial.print(CalDIYTempf4);
      Serial.print(",");
      Serial.print(DIYTempc4ID);  //Temp in Celcius
      Serial.print(":");
      Serial.print(CalDIYTempc4);
      Serial.print(",");
      Serial.print(DIYTempf5ID);  //Temp in Farenheight
      Serial.print(":");
      Serial.print(CalDIYTempf5);
      Serial.print(",");
      Serial.print(DIYTempc5ID);  //Temp in Celcius
      Serial.print(":");
      Serial.print(CalDIYTempc1);
      Serial.print(",");
      Serial.print(DIYTempf6ID);  //Temp in Farenheight
      Serial.print(":");
      Serial.print(CalDIYTempf6);
      Serial.print(",");
      Serial.print(DIYTempc6ID);  //Temp in Celcius
      Serial.print(":");
      Serial.print(CalDIYTempc6);
      Serial.print(",");
      Serial.print(DIYTempf7ID);  //Temp in Farenheight
      Serial.print(":");
      Serial.print(CalDIYTempf7);
      Serial.print(",");
      Serial.print(DIYTempc7ID);  //Temp in Celcius
      Serial.print(":");
      Serial.print(CalDIYTempc7);
      Serial.print(",");
      Serial.print(DIYTempf8ID);  //Not available on 7th order
      Serial.print(":");
      Serial.print(CalDIYTempf8);
      Serial.print(",");
      Serial.print(DIYTempc8ID);  
      Serial.print(":");
      Serial.print(CalDIYTempc8);
      Serial.print(",");     
      Serial.print(FloatSwitch1ID);  //Float Switch Value
      Serial.print(":");
      Serial.print(FloatSwitch1);
      Serial.print(",");
      Serial.print(FloatSwitch2ID);  //Float Switch Value
      Serial.print(":");
      Serial.print(FloatSwitch2);
      Serial.print(",");
      Serial.print(FloatSwitch3ID);  //Float Switch Value
      Serial.print(":");
      Serial.print(FloatSwitch3);
      Serial.print(",");
      Serial.print(FloatSwitch4ID);  //Float Switch Value
      Serial.print(":");
      Serial.print(FloatSwitch4);
      Serial.print(",");
      Serial.print(FloatSwitch5ID);  //Float Switch Value
      Serial.print(":");
      Serial.print(FloatSwitch5);
      Serial.print(",");
      Serial.print(FloatSwitch6ID);  //Float Switch Value
      Serial.print(":");
      Serial.println(FloatSwitch6);
      LastSend = millis();
    }
  }

  if (WatchDogEnable == 1) {
    if (TimeNow > WatchDogTime + 300000) {  //If the arduino hasn't recieved any signal in 5 minutes
        //Initiate Fail-States
      //Triac1 = Triac1FS  
      //blink the leds
      }
    }    


     //Decides what to do when it recieves a serial command
  if (StringComplete) {
    WatchDogTime = millis();  //Check the time
    Comand = InputString.charAt(8) - '0';  //converting char to int
    Comand1 = InputString.charAt(9) - '0';  //converting char 2 to int
    if ((Comand1 >= 0) && (Comand1 <= 9) && (Comand1 != ':')) {
      Comand = Comand * 10;
      Comand += Comand1;
    } 
    switch (Comand) {
     case 0:
        //read device ID from flash memory
        Serial.print(ID);  // Group ID
        Serial.print("/");
        Serial.println(ID);
                 // clear the string:
        InputString = "";
        StringComplete = false;
        Comand = 0;
        Comand1 = 0;
        ActuatorValString = "";
        ActuatorValInt = 0;
        ActuatorValFloat = 0;
        for(int i = 0; i < 8; i++) {
        ActuatorValCharArray[i] = ' ';
        }
        break;  

      case 1:
        //Send Back Sensor Data
        Serial.print("GroupID");
      Serial.print(ID);  //Group ID
      Serial.print("/");
      Serial.print("T1:");  
      Serial.print(analogRead(DIYTemp1Pin));
      Serial.print(",");
      Serial.print("T2:");  
      Serial.print(analogRead(DIYTemp2Pin));
      Serial.print(",");
      Serial.print("T3:");  
      Serial.print(analogRead(DIYTemp3Pin));
      Serial.print(",");
      Serial.print("T4:");  
      Serial.print(analogRead(DIYTemp4Pin));
      Serial.print(",");
      Serial.print("T5:");  
      Serial.print(analogRead(DIYTemp5Pin));
      Serial.print(",");
      Serial.print("T6:");  
      Serial.print(analogRead(DIYTemp6Pin));
      Serial.print(",");
      Serial.print("T7:");  
      Serial.print(analogRead(DIYTemp7Pin));
      Serial.print(",");
      Serial.print("T8:");  
      Serial.print(analogRead(DIYTemp8Pin));
      Serial.print(",");  
      Serial.print("F1:");  //Float Switch Value
      Serial.print(digitalRead(Float1Pin));
      Serial.print(",");
      Serial.print("F2:");  //Float Switch Value
      Serial.print(digitalRead(Float2Pin));
      Serial.print(",");
      Serial.print("F3:");  //Float Switch Value
      Serial.print(digitalRead(Float3Pin));
      Serial.print(",");
      Serial.print("F4:");  //Float Switch Value
      Serial.print(digitalRead(Float4Pin));
      Serial.print(",");
      Serial.print("F5:");  //Float Switch Value
      Serial.print(digitalRead(Float5Pin));
      Serial.print(",");
      Serial.print("F6:");  //Float Switch Value
      Serial.println(digitalRead(Float6Pin));

         // clear the string:
        InputString = "";
        StringComplete = false;
        Comand = 0;
        Comand1 = 0;
        ActuatorValString = "";
        ActuatorValInt = 0;
        ActuatorValFloat = 0;
        for(int i = 0; i < 8; i++) {
        ActuatorValCharArray[i] = ' ';
        }
        break;   

      case 2:
        //Set PWM #1 pin
        calcfunction();
        PWM1 = ActuatorValInt;
        PWM1 = PWM1 * 255 / 100;  //convert to 0-255 for analog write
        if (PWM1 == 0) {    //PWM pin #3&5 don't turn off all the way, this fixes that
          digitalWrite(PWM1Pin, LOW);
          Serial.print(ID);  //Group ID
          Serial.print("/");
          Serial.println("1");  
        }
        else {
          analogWrite(PWM1Pin, PWM1);
          Serial.print(ID);  //Group ID
          Serial.print("/");
          Serial.println("1");   
        }
         // clear the string:
        InputString = "";
        StringComplete = false;
        Comand = 0;
        Comand1 = 0;
        ActuatorValString = "";
        ActuatorValInt = 0;
        ActuatorValFloat = 0;
        for(int i = 0; i < 8; i++) {
        ActuatorValCharArray[i] = ' ';
        }
        break;   
        
      case 3:
        //Set PWM #2 pin
        calcfunction();
        PWM2 = ActuatorValInt;
        PWM2 = PWM2 * 255 / 100;  //convert to 0-255 for analog write
        if (PWM2 == 0) {    //PWM pin #3&5 don't turn off all the way, this fixes that
          digitalWrite(PWM2Pin, LOW);
          Serial.print(ID);  //Group ID
          Serial.print("/");
          Serial.println("1"); 
        }
        else {
          analogWrite(PWM2Pin, PWM2);
          Serial.print(ID);  //Group ID
          Serial.print("/");
          Serial.println("1"); 
        }
         // clear the string:
        InputString = "";
        StringComplete = false;
        Comand = 0;
        Comand1 = 0;
        ActuatorValString = "";
        ActuatorValInt = 0;
        ActuatorValFloat = 0;
        for(int i = 0; i < 8; i++) {
        ActuatorValCharArray[i] = ' ';
        }
        break;   
        
      case 4:
        //Set PWM #3 pin  
        calcfunction();
        PWM3 = ActuatorValInt;
        PWM3 = PWM3 * 255 / 100;  //convert to 0-255 for analog write
        analogWrite(PWM3Pin, PWM3);
        Serial.print(ID);  //Group ID
        Serial.print("/");
        Serial.println("1"); 
         // clear the string:
        InputString = "";
        StringComplete = false;
        Comand = 0;
        Comand1 = 0;
        ActuatorValString = "";
        ActuatorValInt = 0;
        ActuatorValFloat = 0;
        for(int i = 0; i < 8; i++) {
        ActuatorValCharArray[i] = ' ';
        }
        break;   

      case 5:
        //Set PWM #4 pin    
        calcfunction();
        PWM4 = ActuatorValInt;
        PWM4 = PWM4 * 255 / 100;  //convert to 0-255 for analog write
        analogWrite(PWM4Pin, PWM4);
        Serial.print(ID);  //Group ID
        Serial.print("/");
        Serial.println("1"); 
         // clear the string:
        InputString = "";
        StringComplete = false;
        Comand = 0;
        Comand1 = 0;
        ActuatorValString = "";
        ActuatorValInt = 0;
        ActuatorValFloat = 0;
        for(int i = 0; i < 8; i++) {
        ActuatorValCharArray[i] = ' ';
        }
        break;   

      case 6:
        //Set PWM #5
        calcfunction();
        PWM5 = ActuatorValInt;
        PWM5 = PWM5 * 255 / 100;  //convert to 0-255 for analog write
        analogWrite(PWM5Pin, PWM5);
        Serial.print(ID);  //Group ID
        Serial.print("/");
        Serial.println("1"); 
         // clear the string:
        InputString = "";
        StringComplete = false;
        Comand = 0;
        Comand1 = 0;
        ActuatorValString = "";
        ActuatorValInt = 0;
        ActuatorValFloat = 0;
        for(int i = 0; i < 8; i++) {
        ActuatorValCharArray[i] = ' ';
        }
        break;   

      case 7:
        //Set PWM #6
        calcfunction();
        PWM6 = ActuatorValInt;
        PWM6 = PWM6 * 255 / 100;  //convert to 0-255 for analog write
        analogWrite(PWM6Pin, PWM6);
        Serial.print(ID);  //Group ID
        Serial.print("/");
        Serial.println("1"); 
         // clear the string:
        InputString = "";
        StringComplete = false;
        Comand = 0;
        Comand1 = 0;
        ActuatorValString = "";
        ActuatorValInt = 0;
        ActuatorValFloat = 0;
        for(int i = 0; i < 8; i++) {
        ActuatorValCharArray[i] = ' ';
        }
        break;   
        
      case 10: 
         //Send Back Sensor Data to Ai
      Serial.print(ID);  //Group ID
      Serial.print("/");
      Serial.print(DIYTempf1ID);  //Temp in Farenheight
      Serial.print(":");
      Serial.print(CalDIYTempf1);
      Serial.print(",");
      Serial.print(DIYTempc1ID);  //Temp in Celcius
      Serial.print(":");
      Serial.print(CalDIYTempc1);
      Serial.print(",");
      Serial.print(DIYTempf2ID);  //Temp in Farenheight
      Serial.print(":");
      Serial.print(CalDIYTempf2);
      Serial.print(",");
      Serial.print(DIYTempc2ID);  //Temp in Celcius
      Serial.print(":");
      Serial.print(CalDIYTempc2);
      Serial.print(",");
      Serial.print(DIYTempf3ID);  //Temp in Farenheight
      Serial.print(":");
      Serial.print(CalDIYTempf3);
      Serial.print(",");
      Serial.print(DIYTempc3ID);  //Temp in Celcius
      Serial.print(":");
      Serial.print(CalDIYTempc3);
      Serial.print(",");
      Serial.print(DIYTempf4ID);  //Temp in Farenheight
      Serial.print(":");
      Serial.print(CalDIYTempf4);
      Serial.print(",");
      Serial.print(DIYTempc4ID);  //Temp in Celcius
      Serial.print(":");
      Serial.print(CalDIYTempc4);
      Serial.print(",");
      Serial.print(DIYTempf5ID);  //Temp in Farenheight
      Serial.print(":");
      Serial.print(CalDIYTempf5);
      Serial.print(",");
      Serial.print(DIYTempc5ID);  //Temp in Celcius
      Serial.print(":");
      Serial.print(CalDIYTempc1);
      Serial.print(",");
      Serial.print(DIYTempf6ID);  //Temp in Farenheight
      Serial.print(":");
      Serial.print(CalDIYTempf6);
      Serial.print(",");
      Serial.print(DIYTempc6ID);  //Temp in Celcius
      Serial.print(":");
      Serial.print(CalDIYTempc6);
      Serial.print(",");
      Serial.print(DIYTempf7ID);  //Temp in Farenheight
      Serial.print(":");
      Serial.print(CalDIYTempf7);
      Serial.print(",");
      Serial.print(DIYTempc7ID);  //Temp in Celcius
      Serial.print(":");
      Serial.print(CalDIYTempc7);
      Serial.print(",");
      Serial.print(DIYTempf8ID);  //Not available on 7th order
      Serial.print(":");
      Serial.print(CalDIYTempf8);
      Serial.print(",");
      Serial.print(DIYTempc8ID);  
      Serial.print(":");
      Serial.print(CalDIYTempc8);
      Serial.print(",");     
      Serial.print(FloatSwitch1ID);  //Float Switch Value
      Serial.print(":");
      Serial.print(FloatSwitch1);
      Serial.print(",");
      Serial.print(FloatSwitch2ID);  //Float Switch Value
      Serial.print(":");
      Serial.print(FloatSwitch2);
      Serial.print(",");
      Serial.print(FloatSwitch3ID);  //Float Switch Value
      Serial.print(":");
      Serial.print(FloatSwitch3);
      Serial.print(",");
      Serial.print(FloatSwitch4ID);  //Float Switch Value
      Serial.print(":");
      Serial.print(FloatSwitch4);
      Serial.print(",");
      Serial.print(FloatSwitch5ID);  //Float Switch Value
      Serial.print(":");
      Serial.print(FloatSwitch5);
      Serial.print(",");
      Serial.print(FloatSwitch6ID);  //Float Switch Value
      Serial.print(":");
      Serial.println(FloatSwitch6);
       // clear the string:
        InputString = "";
        StringComplete = false;
        Comand = 0;
        Comand1 = 0;
        ActuatorValString = "";
        ActuatorValInt = 0;
        ActuatorValFloat = 0;
        for(int i = 0; i < 8; i++) {
        ActuatorValCharArray[i] = ' ';
        }
        break;   
      

      case 11:
          //Send Back Data for 60 Seconds
        calcfunction();
        SendDelay = ActuatorValInt;  //Grab the time in milliseconds
        StartTime = millis();                   //to delay before sending again
        SendData = true;
        SendQue = true;
                   // clear the string:
        InputString = "";
        StringComplete = false;
        Comand = 0;
        Comand1 = 0;
        ActuatorValString = "";
        ActuatorValInt = 0;
        ActuatorValFloat = 0;
        for(int i = 0; i < 8; i++) {
        ActuatorValCharArray[i] = ' ';
        }
        break;     
        
      case 13:
          //Send Back Actuator Data
        Serial.print(ID);  //Group ID
        Serial.print("/");
        Serial.print(PWM1ID);
        Serial.print(":");
        Serial.print(PWM1);
        Serial.print(",");
        Serial.print(PWM2ID);
        Serial.print(":");
        Serial.print(PWM2);
        Serial.print(",");
        Serial.print(PWM3ID);
        Serial.print(":");
        Serial.print(PWM3);
        Serial.print(",");
        Serial.print(PWM4ID);
        Serial.print(":");
        Serial.print(PWM4);
        Serial.print(",");
        Serial.print(PWM5ID);
        Serial.print(":");
        Serial.print(PWM5);
        Serial.print(",");
        Serial.print(PWM6ID);
        Serial.print(":");
        Serial.println(PWM6);
        
        // clear the string:
        InputString = "";
        StringComplete = false;
        Comand = 0;
        Comand1 = 0;
        ActuatorValString = "";
        ActuatorValInt = 0;
        ActuatorValFloat = 0;
        for(int i = 0; i < 8; i++) {
        ActuatorValCharArray[i] = ' ';
        }
        break; 

      case 14: 
          //Write LED Brightness Value (0-100%)  **Doesn't Do anything on this board**
        Serial.print(ID);  //Group ID
        Serial.print("/");
        Serial.println("1"); 
                   // clear the string:
        InputString = "";
        StringComplete = false;
        Comand = 0;
        Comand1 = 0;
        ActuatorValString = "";
        ActuatorValInt = 0;
        ActuatorValFloat = 0;
        for(int i = 0; i < 8; i++) {
        ActuatorValCharArray[i] = ' ';
        }
        break; 

      case 17:
        //Enable/Disable Watchdog timer (1/0)
        calcfunction();
        EEPROM.writeInt(360, ActuatorValInt);  //Save value to EEPROM
        WatchDogEnable = ActuatorValInt;
        Serial.print(ID);  //Group ID
        Serial.print("/");
        Serial.println("1"); 
                 // clear the string:
        InputString = "";
        StringComplete = false;
        Comand = 0;
        Comand1 = 0;
        ActuatorValString = "";
        ActuatorValInt = 0;
        ActuatorValFloat = 0;
        for(int i = 0; i < 8; i++) {
        ActuatorValCharArray[i] = ' ';
        }
        break; 
      
      case 20: 
          //Write PWM #1 Fail Safe Value (0-100%)
        calcfunction();
        EEPROM.writeInt(362, ActuatorValInt);  //Save value to EEPROM
        PWM1FS = ActuatorValInt;
        Serial.print(ID);  //Group ID
        Serial.print("/");
        Serial.println("1"); 
                   // clear the string:
        InputString = "";
        StringComplete = false;
        Comand = 0;
        Comand1 = 0;
        ActuatorValString = "";
        ActuatorValInt = 0;
        ActuatorValFloat = 0;
        for(int i = 0; i < 8; i++) {
        ActuatorValCharArray[i] = ' ';
        }
        break; 
        
      case 21: 
          //Write PWM #2 Fail Safe Value (0-100%)
        calcfunction();
        EEPROM.writeInt(364, ActuatorValInt);  //Save value to EEPROM
        PWM1FS = ActuatorValInt;
        Serial.print(ID);  //Group ID
        Serial.print("/");
        Serial.println("1"); 
                   // clear the string:
        InputString = "";
        StringComplete = false;
        Comand = 0;
        Comand1 = 0;
        ActuatorValString = "";
        ActuatorValInt = 0;
        ActuatorValFloat = 0;
        for(int i = 0; i < 8; i++) {
        ActuatorValCharArray[i] = ' ';
        }
        break; 
        
      case 22: 
          //Write PWM #3 Fail Safe Value (0-100%)
        calcfunction();
        EEPROM.writeInt(366, ActuatorValInt);  //Save value to EEPROM
        PWM1FS = ActuatorValInt;
        Serial.print(ID);  //Group ID
        Serial.print("/");
        Serial.println("1"); 
                   // clear the string:
        InputString = "";
        StringComplete = false;
        Comand = 0;
        Comand1 = 0;
        ActuatorValString = "";
        ActuatorValInt = 0;
        ActuatorValFloat = 0;
        for(int i = 0; i < 8; i++) {
        ActuatorValCharArray[i] = ' ';
        }
        break; 

    case 23: 
          //Write PWM #4 Fail Safe Value (0-100%)
        calcfunction();
        EEPROM.writeInt(368, ActuatorValInt);  //Save value to EEPROM
        PWM1FS = ActuatorValInt;
        Serial.print(ID);  //Group ID
        Serial.print("/");
        Serial.println("1"); 
                   // clear the string:
        InputString = "";
        StringComplete = false;
        Comand = 0;
        Comand1 = 0;
        ActuatorValString = "";
        ActuatorValInt = 0;
        ActuatorValFloat = 0;
        for(int i = 0; i < 8; i++) {
        ActuatorValCharArray[i] = ' ';
        }
        break;     
      
      case 24: 
          //Write PWM #5 Fail Safe Value (0-100%)
        calcfunction();
        EEPROM.writeInt(370, ActuatorValInt);  //Save value to EEPROM
        PWM1FS = ActuatorValInt;
        Serial.print(ID);  //Group ID
        Serial.print("/");
        Serial.println("1"); 
                   // clear the string:
        InputString = "";
        StringComplete = false;
        Comand = 0;
        Comand1 = 0;
        ActuatorValString = "";
        ActuatorValInt = 0;
        ActuatorValFloat = 0;
        for(int i = 0; i < 8; i++) {
        ActuatorValCharArray[i] = ' ';
        }
        break; 
        
      case 25: 
          //Write PWM #6 Fail Safe Value (0-100%)
        calcfunction();
        EEPROM.writeInt(372, ActuatorValInt);  //Save value to EEPROM
        PWM1FS = ActuatorValInt;
        Serial.print(ID);  //Group ID
        Serial.print("/");
        Serial.println("1"); 
                   // clear the string:
        InputString = "";
        StringComplete = false;
        Comand = 0;
        Comand1 = 0;
        ActuatorValString = "";
        ActuatorValInt = 0;
        ActuatorValFloat = 0;
        for(int i = 0; i < 8; i++) {
        ActuatorValCharArray[i] = ' ';
        }
        break; 

      case 30:
          //Write Group ID to EEPROM
        calcfunction();
        EEPROM.writeBlock<char>(0, ActuatorValCharArray, 8);
        //Read ID
        EEPROM.readBlock<char>(0, ID, 8);
        Serial.print(ID);  //Group ID
        Serial.print("/");
        Serial.println("1"); 
                 // clear the string:
        InputString = "";
        StringComplete = false;
        Comand = 0;
        Comand1 = 0;
        ActuatorValString = "";
        ActuatorValInt = 0;
        ActuatorValFloat = 0;
        for(int i = 0; i < 8; i++) {
        ActuatorValCharArray[i] = ' ';
        }
        break; 

      case 31:  
        //Write 1st Element ID to EEPROM *DIYTempf1*
        calcfunction();
        EEPROM.writeBlock<char>(8, ActuatorValCharArray, 8);
        //Read ID
        EEPROM.readBlock<char>(8, DIYTempf1ID, 8);
        Serial.print(ID);  //Group ID
        Serial.print("/");
        Serial.println("1"); 
                 // clear the string:
        InputString = "";
        StringComplete = false;
        Comand = 0;
        Comand1 = 0;
        ActuatorValString = "";
        ActuatorValInt = 0;
        ActuatorValFloat = 0;
        for(int i = 0; i < 8; i++) {
        ActuatorValCharArray[i] = ' ';
        }
        break; 
        
      case 32:  
        //Write 2nd Element ID to EEPROM *DIYTempc1*
        calcfunction();
        EEPROM.writeBlock<char>(24, ActuatorValCharArray, 8);
        //Read ID
        EEPROM.readBlock<char>(24, DIYTempc1ID, 8);
        Serial.print(ID);  //Group ID
        Serial.print("/");
        Serial.println("1"); 
                 // clear the string:
        InputString = "";
        StringComplete = false;
        Comand = 0;
        Comand1 = 0;
        ActuatorValString = "";
        ActuatorValInt = 0;
        ActuatorValFloat = 0;
        for(int i = 0; i < 8; i++) {
        ActuatorValCharArray[i] = ' ';
        }
        break; 
        
      case 33:  
        //Write 3rd Element ID to EEPROM *DIYTempf2*
        calcfunction();
        EEPROM.writeBlock<char>(40, ActuatorValCharArray, 8);
        //Read ID
        EEPROM.readBlock<char>(40, DIYTempf2ID, 8);
        Serial.print(ID);  //Group ID
        Serial.print("/");
        Serial.println("1"); 
                 // clear the string:
        InputString = "";
        StringComplete = false;
        Comand = 0;
        Comand1 = 0;
        ActuatorValString = "";
        ActuatorValInt = 0;
        ActuatorValFloat = 0;
        for(int i = 0; i < 8; i++) {
        ActuatorValCharArray[i] = ' ';
        }
        break; 
        
      case 34:  
        //Write 4th Element ID to EEPROM *DIYTempc2*
        calcfunction();
        EEPROM.writeBlock<char>(56, ActuatorValCharArray, 8);
        //Read ID
        EEPROM.readBlock<char>(56, DIYTempc2ID, 8);
        Serial.print(ID);  //Group ID
        Serial.print("/");
        Serial.println("1"); 
                 // clear the string:
        InputString = "";
        StringComplete = false;
        Comand = 0;
        Comand1 = 0;
        ActuatorValString = "";
        ActuatorValInt = 0;
        ActuatorValFloat = 0;
        for(int i = 0; i < 8; i++) {
        ActuatorValCharArray[i] = ' ';
        }
        break; 
        
      case 35:  
        //Write 5th Element ID to EEPROM *DIYTempf3*
        calcfunction();
        EEPROM.writeBlock<char>(72, ActuatorValCharArray, 8);
        //Read ID
        EEPROM.readBlock<char>(72, DIYTempf3ID, 8);
        Serial.print(ID);  //Group ID
        Serial.print("/");
        Serial.println("1"); 
                 // clear the string:
        InputString = "";
        StringComplete = false;
        Comand = 0;
        Comand1 = 0;
        ActuatorValString = "";
        ActuatorValInt = 0;
        ActuatorValFloat = 0;
        for(int i = 0; i < 8; i++) {
        ActuatorValCharArray[i] = ' ';
        }
        break; 
        
      case 36:  
        //Write 6th Element ID to EEPROM *DIYTempc3*
        calcfunction();
        EEPROM.writeBlock<char>(88, ActuatorValCharArray, 8);
        //Read ID
        EEPROM.readBlock<char>(88, DIYTempc3ID, 8);
        Serial.print(ID);  //Group ID
        Serial.print("/");
        Serial.println("1"); 
                 // clear the string:
        InputString = "";
        StringComplete = false;
        Comand = 0;
        Comand1 = 0;
        ActuatorValString = "";
        ActuatorValInt = 0;
        ActuatorValFloat = 0;
        for(int i = 0; i < 8; i++) {
        ActuatorValCharArray[i] = ' ';
        }
        break; 

      case 37:  
        //Write 7th Element ID to EEPROM *DIYTempf4*
        calcfunction();
        EEPROM.writeBlock<char>(104, ActuatorValCharArray, 8);
        //Read ID
        EEPROM.readBlock<char>(104, DIYTempf4ID, 8);
        Serial.print(ID);  //Group ID
        Serial.print("/");
        Serial.println("1"); 
                 // clear the string:
        InputString = "";
        StringComplete = false;
        Comand = 0;
        Comand1 = 0;
        ActuatorValString = "";
        ActuatorValInt = 0;
        ActuatorValFloat = 0;
        for(int i = 0; i < 8; i++) {
        ActuatorValCharArray[i] = ' ';
        }
        break; 
        
      case 38:  
        //Write 8th Element ID to EEPROM *DIYTempc4*
        calcfunction();
        EEPROM.writeBlock<char>(120, ActuatorValCharArray, 8);
        //Read ID
        EEPROM.readBlock<char>(120, DIYTempc4ID, 8);
        Serial.print(ID);  //Group ID
        Serial.print("/");
        Serial.println("1"); 
                 // clear the string:
        InputString = "";
        StringComplete = false;
        Comand = 0;
        Comand1 = 0;
        ActuatorValString = "";
        ActuatorValInt = 0;
        ActuatorValFloat = 0;
        for(int i = 0; i < 8; i++) {
        ActuatorValCharArray[i] = ' ';
        }
        break;
        
      case 39:  
        //Write 9th Element ID to EEPROM *DIYTempf5*
        calcfunction();
        EEPROM.writeBlock<char>(136, ActuatorValCharArray, 8);
        //Read ID
        EEPROM.readBlock<char>(136, DIYTempf5ID, 8);
        Serial.print(ID);  //Group ID
        Serial.print("/");
        Serial.println("1"); 
                 // clear the string:
        InputString = "";
        StringComplete = false;
        Comand = 0;
        Comand1 = 0;
        ActuatorValString = "";
        ActuatorValInt = 0;
        ActuatorValFloat = 0;
        for(int i = 0; i < 8; i++) {
        ActuatorValCharArray[i] = ' ';
        }
        break; 
        
      case 40:  
        //Write 10th Element ID to EEPROM *DIYTempc5*
        calcfunction();
        EEPROM.writeBlock<char>(152, ActuatorValCharArray, 8);
        //Read ID
        EEPROM.readBlock<char>(152, DIYTempc5ID, 8);
        Serial.print(ID);  //Group ID
        Serial.print("/");
        Serial.println("1"); 
                 // clear the string:
        InputString = "";
        StringComplete = false;
        Comand = 0;
        Comand1 = 0;
        ActuatorValString = "";
        ActuatorValInt = 0;
        ActuatorValFloat = 0;
        for(int i = 0; i < 8; i++) {
        ActuatorValCharArray[i] = ' ';
        }
        break;
        
      case 41:  
        //Write 11th Element ID to EEPROM *DIYTempf6*
        calcfunction();
        EEPROM.writeBlock<char>(168, ActuatorValCharArray, 8);
        //Read ID
        EEPROM.readBlock<char>(168, DIYTempf6ID, 8);
        Serial.print(ID);  //Group ID
        Serial.print("/");
        Serial.println("1"); 
                 // clear the string:
        InputString = "";
        StringComplete = false;
        Comand = 0;
        Comand1 = 0;
        ActuatorValString = "";
        ActuatorValInt = 0;
        ActuatorValFloat = 0;
        for(int i = 0; i < 8; i++) {
        ActuatorValCharArray[i] = ' ';
        }
        break; 
        
      case 42:  
        //Write 12th Element ID to EEPROM *DIYTempc6*
        calcfunction();
        EEPROM.writeBlock<char>(184, ActuatorValCharArray, 8);
        //Read ID
        EEPROM.readBlock<char>(184, DIYTempc6ID, 8);
        Serial.print(ID);  //Group ID
        Serial.print("/");
        Serial.println("1"); 
                 // clear the string:
        InputString = "";
        StringComplete = false;
        Comand = 0;
        Comand1 = 0;
        ActuatorValString = "";
        ActuatorValInt = 0;
        ActuatorValFloat = 0;
        for(int i = 0; i < 8; i++) {
        ActuatorValCharArray[i] = ' ';
        }
        break;
        
      case 43:  
        //Write 13th Element ID to EEPROM *DIYTempf7*
        calcfunction();
        EEPROM.writeBlock<char>(200, ActuatorValCharArray, 8);
        //Read ID
        EEPROM.readBlock<char>(200, DIYTempf7ID, 8);
        Serial.print(ID);  //Group ID
        Serial.print("/");
        Serial.println("1"); 
                 // clear the string:
        InputString = "";
        StringComplete = false;
        Comand = 0;
        Comand1 = 0;
        ActuatorValString = "";
        ActuatorValInt = 0;
        ActuatorValFloat = 0;
        for(int i = 0; i < 8; i++) {
        ActuatorValCharArray[i] = ' ';
        }
        break; 
        
      case 44:  
        //Write 14th Element ID to EEPROM *DIYTempc7*
        calcfunction();
        EEPROM.writeBlock<char>(216, ActuatorValCharArray, 8);
        //Read ID
        EEPROM.readBlock<char>(216, DIYTempc7ID, 8);
        Serial.print(ID);  //Group ID
        Serial.print("/");
        Serial.println("1"); 
                 // clear the string:
        InputString = "";
        StringComplete = false;
        Comand = 0;
        Comand1 = 0;
        ActuatorValString = "";
        ActuatorValInt = 0;
        ActuatorValFloat = 0;
        for(int i = 0; i < 8; i++) {
        ActuatorValCharArray[i] = ' ';
        }
        break;
        
      case 45:  
        //Write 15th Element ID to EEPROM *DIYTempf8*
        calcfunction();
        EEPROM.writeBlock<char>(232, ActuatorValCharArray, 8);
        //Read ID
        EEPROM.readBlock<char>(232, DIYTempf8ID, 8);
        Serial.print(ID);  //Group ID
        Serial.print("/");
        Serial.println("1"); 
                 // clear the string:
        InputString = "";
        StringComplete = false;
        Comand = 0;
        Comand1 = 0;
        ActuatorValString = "";
        ActuatorValInt = 0;
        ActuatorValFloat = 0;
        for(int i = 0; i < 8; i++) {
        ActuatorValCharArray[i] = ' ';
        }
        break; 
        
      case 46:  
        //Write 16th Element ID to EEPROM *DIYTempc8*
        calcfunction();
        EEPROM.writeBlock<char>(248, ActuatorValCharArray, 8);
        //Read ID
        EEPROM.readBlock<char>(248, DIYTempc8ID, 8);
        Serial.print(ID);  //Group ID
        Serial.print("/");
        Serial.println("1"); 
                 // clear the string:
        InputString = "";
        StringComplete = false;
        Comand = 0;
        Comand1 = 0;
        ActuatorValString = "";
        ActuatorValInt = 0;
        ActuatorValFloat = 0;
        for(int i = 0; i < 8; i++) {
        ActuatorValCharArray[i] = ' ';
        }
        break;
        
      case 47:  
        //Write 17th Element ID to EEPROM *Float Switch #1*
        calcfunction();
        EEPROM.writeBlock<char>(264, ActuatorValCharArray, 8);
        //Read ID
        EEPROM.readBlock<char>(264, FloatSwitch1ID, 8);
        Serial.print(ID);  //Group ID
        Serial.print("/");
        Serial.println("1"); 
                 // clear the string:
        InputString = "";
        StringComplete = false;
        Comand = 0;
        Comand1 = 0;
        ActuatorValString = "";
        ActuatorValInt = 0;
        ActuatorValFloat = 0;
        for(int i = 0; i < 8; i++) {
        ActuatorValCharArray[i] = ' ';
        }
        break; 
        
      case 48:  
        //Write 18th Element ID to EEPROM *Float Switch #2*
        calcfunction();
        EEPROM.writeBlock<char>(272, ActuatorValCharArray, 8);
        //Read ID
        EEPROM.readBlock<char>(272, FloatSwitch2ID, 8);
        Serial.print(ID);  //Group ID
        Serial.print("/");
        Serial.println("1"); 
                 // clear the string:
        InputString = "";
        StringComplete = false;
        Comand = 0;
        Comand1 = 0;
        ActuatorValString = "";
        ActuatorValInt = 0;
        ActuatorValFloat = 0;
        for(int i = 0; i < 8; i++) {
        ActuatorValCharArray[i] = ' ';
        }
        break;
        
      case 49:  
        //Write 19th Element ID to EEPROM *Float Switch #3*
        calcfunction();
        EEPROM.writeBlock<char>(280, ActuatorValCharArray, 8);
        //Read ID
        EEPROM.readBlock<char>(280, FloatSwitch3ID, 8);
        Serial.print(ID);  //Group ID
        Serial.print("/");
        Serial.println("1"); 
                 // clear the string:
        InputString = "";
        StringComplete = false;
        Comand = 0;
        Comand1 = 0;
        ActuatorValString = "";
        ActuatorValInt = 0;
        ActuatorValFloat = 0;
        for(int i = 0; i < 8; i++) {
        ActuatorValCharArray[i] = ' ';
        }
        break; 
        
      case 50:  
        //Write 20th Element ID to EEPROM *Float Switch #4*
        calcfunction();
        EEPROM.writeBlock<char>(288, ActuatorValCharArray, 8);
        //Read ID
        EEPROM.readBlock<char>(288, FloatSwitch4ID, 8);
        Serial.print(ID);  //Group ID
        Serial.print("/");
        Serial.println("1"); 
                 // clear the string:
        InputString = "";
        StringComplete = false;
        Comand = 0;
        Comand1 = 0;
        ActuatorValString = "";
        ActuatorValInt = 0;
        ActuatorValFloat = 0;
        for(int i = 0; i < 8; i++) {
        ActuatorValCharArray[i] = ' ';
        }
        break;
        
      case 51:  
        //Write 21st Element ID to EEPROM *Float Switch #5*
        calcfunction();
        EEPROM.writeBlock<char>(296, ActuatorValCharArray, 8);
        //Read ID
        EEPROM.readBlock<char>(296, FloatSwitch5ID, 8);
        Serial.print(ID);  //Group ID
        Serial.print("/");
        Serial.println("1"); 
                 // clear the string:
        InputString = "";
        StringComplete = false;
        Comand = 0;
        Comand1 = 0;
        ActuatorValString = "";
        ActuatorValInt = 0;
        ActuatorValFloat = 0;
        for(int i = 0; i < 8; i++) {
        ActuatorValCharArray[i] = ' ';
        }
        break; 
        
      case 52:  
        //Write 22nd Element ID to EEPROM *Float Switch #6*
        calcfunction();
        EEPROM.writeBlock<char>(304, ActuatorValCharArray, 8);
        //Read ID
        EEPROM.readBlock<char>(304, FloatSwitch6ID, 8);
        Serial.print(ID);  //Group ID
        Serial.print("/");
        Serial.println("1"); 
                 // clear the string:
        InputString = "";
        StringComplete = false;
        Comand = 0;
        Comand1 = 0;
        ActuatorValString = "";
        ActuatorValInt = 0;
        ActuatorValFloat = 0;
        for(int i = 0; i < 8; i++) {
        ActuatorValCharArray[i] = ' ';
        }
        break;
        
      case 53:  
        //Write 23rd Element ID to EEPROM *PWM #1*
        calcfunction();
        EEPROM.writeBlock<char>(312, ActuatorValCharArray, 8);
        //Read ID
        EEPROM.readBlock<char>(312, PWM1ID, 8);
        Serial.print(ID);  //Group ID
        Serial.print("/");
        Serial.println("1"); 
                 // clear the string:
        InputString = "";
        StringComplete = false;
        Comand = 0;
        Comand1 = 0;
        ActuatorValString = "";
        ActuatorValInt = 0;
        ActuatorValFloat = 0;
        for(int i = 0; i < 8; i++) {
        ActuatorValCharArray[i] = ' ';
        }
        break;
        
      case 54:  
        //Write 24th Element ID to EEPROM *PWM #2*
        calcfunction();
        EEPROM.writeBlock<char>(320, ActuatorValCharArray, 8);
        //Read ID
        EEPROM.readBlock<char>(320, PWM2ID, 8);
        Serial.print(ID);  //Group ID
        Serial.print("/");
        Serial.println("1"); 
                 // clear the string:
        InputString = "";
        StringComplete = false;
        Comand = 0;
        Comand1 = 0;
        ActuatorValString = "";
        ActuatorValInt = 0;
        ActuatorValFloat = 0;
        for(int i = 0; i < 8; i++) {
        ActuatorValCharArray[i] = ' ';
        }
        break;
        
      case 55:  
        //Write 25th Element ID to EEPROM *PWM #3*
        calcfunction();
        EEPROM.writeBlock<char>(328, ActuatorValCharArray, 8);
        //Read ID
        EEPROM.readBlock<char>(328, PWM3ID, 8);
        Serial.print(ID);  //Group ID
        Serial.print("/");
        Serial.println("1"); 
                 // clear the string:
        InputString = "";
        StringComplete = false;
        Comand = 0;
        Comand1 = 0;
        ActuatorValString = "";
        ActuatorValInt = 0;
        ActuatorValFloat = 0;
        for(int i = 0; i < 8; i++) {
        ActuatorValCharArray[i] = ' ';
        }
        break;

      case 56:  
        //Write 26th Element ID to EEPROM *PWM #4*
        calcfunction();
        EEPROM.writeBlock<char>(336, ActuatorValCharArray, 8);
        //Read ID
        EEPROM.readBlock<char>(336, PWM4ID, 8);
        Serial.print(ID);  //Group ID
        Serial.print("/");
        Serial.println("1"); 
                 // clear the string:
        InputString = "";
        StringComplete = false;
        Comand = 0;
        Comand1 = 0;
        ActuatorValString = "";
        ActuatorValInt = 0;
        ActuatorValFloat = 0;
        for(int i = 0; i < 8; i++) {
        ActuatorValCharArray[i] = ' ';
        }
        break;
        
      case 57:  
        //Write 27th Element ID to EEPROM *PWM #5*
        calcfunction();
        EEPROM.writeBlock<char>(344, ActuatorValCharArray, 8);
        //Read ID
        EEPROM.readBlock<char>(344, PWM5ID, 8);
        Serial.print(ID);  //Group ID
        Serial.print("/");
        Serial.println("1"); 
                 // clear the string:
        InputString = "";
        StringComplete = false;
        Comand = 0;
        Comand1 = 0;
        ActuatorValString = "";
        ActuatorValInt = 0;
        ActuatorValFloat = 0;
        for(int i = 0; i < 8; i++) {
        ActuatorValCharArray[i] = ' ';
        }
        break;
        
      case 58:  
        //Write 28th Element ID to EEPROM *PWM #6*
        calcfunction();
        EEPROM.writeBlock<char>(352, ActuatorValCharArray, 8);
        //Read ID
        EEPROM.readBlock<char>(352, PWM6ID, 8);
        Serial.print(ID);  //Group ID
        Serial.print("/");
        Serial.println("1"); 
                 // clear the string:
        InputString = "";
        StringComplete = false;
        Comand = 0;
        Comand1 = 0;
        ActuatorValString = "";
        ActuatorValInt = 0;
        ActuatorValFloat = 0;
        for(int i = 0; i < 8; i++) {
        ActuatorValCharArray[i] = ' ';
        }
        break;
        
      case 60:
        //Write 1st Element Calibration Slope (M) to EEPROM *DIYTempf1* 
        calcfunction();
        EEPROM.writeFloat(16, ActuatorValFloat);  //Save value to EEPROM
        DIYTemp1CalM = ActuatorValFloat;
        Serial.print(ID);  //Group ID
        Serial.print("/");
        Serial.println("1"); 
        
                 // clear the string:
        InputString = "";
        StringComplete = false;
        Comand = 0;
        Comand1 = 0;
        ActuatorValString = "";
        ActuatorValInt = 0;
        ActuatorValFloat = 0;
        for(int i = 0; i < 8; i++) {
        ActuatorValCharArray[i] = ' ';
        }
        break; 
        
      case 61:
        //Write 1st Element Calibration Y-inter (B) to EEPROM *DIYTempf1*
        calcfunction();
        EEPROM.writeFloat(20, ActuatorValFloat);  //Save value to EEPROM
        DIYTemp1CalB = ActuatorValFloat;
        Serial.print(ID);  //Group ID
        Serial.print("/");
        Serial.println("1"); 
                 // clear the string:
        InputString = "";
        StringComplete = false;
        Comand = 0;
        Comand1 = 0;
        ActuatorValString = "";
        ActuatorValInt = 0;
        ActuatorValFloat = 0;
        for(int i = 0; i < 8; i++) {
        ActuatorValCharArray[i] = ' ';
        }
        break; 
        
      case 62:
        //Write 3rd Element Calibration Slope (M) to EEPROM *DIYTempf2* 
        calcfunction();
        EEPROM.writeFloat(48, ActuatorValFloat);  //Save value to EEPROM
        DIYTemp2CalM = ActuatorValFloat;
        Serial.print(ID);  //Group ID
        Serial.print("/");
        Serial.println("1"); 
        
                 // clear the string:
        InputString = "";
        StringComplete = false;
        Comand = 0;
        Comand1 = 0;
        ActuatorValString = "";
        ActuatorValInt = 0;
        ActuatorValFloat = 0;
        for(int i = 0; i < 8; i++) {
        ActuatorValCharArray[i] = ' ';
        }
        break; 
        
      case 63:
        //Write 3rd Element Calibration Y-inter (B) to EEPROM *DIYTempf2*
        calcfunction();
        EEPROM.writeFloat(52, ActuatorValFloat);  //Save value to EEPROM
        DIYTemp2CalB = ActuatorValFloat;
        Serial.print(ID);  //Group ID
        Serial.print("/");
        Serial.println("1"); 
                 // clear the string:
        InputString = "";
        StringComplete = false;
        Comand = 0;
        Comand1 = 0;
        ActuatorValString = "";
        ActuatorValInt = 0;
        ActuatorValFloat = 0;
        for(int i = 0; i < 8; i++) {
        ActuatorValCharArray[i] = ' ';
        }
        break; 
        
      case 64:
        //Write 5th Element Calibration Slope (M) to EEPROM *DIYTempf3* 
        calcfunction();
        EEPROM.writeFloat(80, ActuatorValFloat);  //Save value to EEPROM
        DIYTemp3CalM = ActuatorValFloat;
        Serial.print(ID);  //Group ID
        Serial.print("/");
        Serial.println("1"); 
        
                 // clear the string:
        InputString = "";
        StringComplete = false;
        Comand = 0;
        Comand1 = 0;
        ActuatorValString = "";
        ActuatorValInt = 0;
        ActuatorValFloat = 0;
        for(int i = 0; i < 8; i++) {
        ActuatorValCharArray[i] = ' ';
        }
        break; 
        
      case 65:
        //Write 5th Element Calibration Y-inter (B) to EEPROM *DIYTempf3*
        calcfunction();
        EEPROM.writeFloat(84, ActuatorValFloat);  //Save value to EEPROM
        DIYTemp3CalB = ActuatorValFloat;
        Serial.print(ID);  //Group ID
        Serial.print("/");
        Serial.println("1"); 
                 // clear the string:
        InputString = "";
        StringComplete = false;
        Comand = 0;
        Comand1 = 0;
        ActuatorValString = "";
        ActuatorValInt = 0;
        ActuatorValFloat = 0;
        for(int i = 0; i < 8; i++) {
        ActuatorValCharArray[i] = ' ';
        }
        break; 
       
      case 66:
        //Write 7th Element Calibration Slope (M) to EEPROM *DIYTempf4* 
        calcfunction();
        EEPROM.writeFloat(112, ActuatorValFloat);  //Save value to EEPROM
        DIYTemp4CalM = ActuatorValFloat;
        Serial.print(ID);  //Group ID
        Serial.print("/");
        Serial.println("1"); 
        
                 // clear the string:
        InputString = "";
        StringComplete = false;
        Comand = 0;
        Comand1 = 0;
        ActuatorValString = "";
        ActuatorValInt = 0;
        ActuatorValFloat = 0;
        for(int i = 0; i < 8; i++) {
        ActuatorValCharArray[i] = ' ';
        }
        break; 
        
      case 67:
        //Write 7th Element Calibration Y-inter (B) to EEPROM *DIYTempf4*
        calcfunction();
        EEPROM.writeFloat(116, ActuatorValFloat);  //Save value to EEPROM
        DIYTemp4CalB = ActuatorValFloat;
        Serial.print(ID);  //Group ID
        Serial.print("/");
        Serial.println("1"); 
                 // clear the string:
        InputString = "";
        StringComplete = false;
        Comand = 0;
        Comand1 = 0;
        ActuatorValString = "";
        ActuatorValInt = 0;
        ActuatorValFloat = 0;
        for(int i = 0; i < 8; i++) {
        ActuatorValCharArray[i] = ' ';
        }
        break; 
        
      case 68:
        //Write 9th Element Calibration Slope (M) to EEPROM *DIYTempf5* 
        calcfunction();
        EEPROM.writeFloat(144, ActuatorValFloat);  //Save value to EEPROM
        DIYTemp5CalM = ActuatorValFloat;
        Serial.print(ID);  //Group ID
        Serial.print("/");
        Serial.println("1"); 
        
                 // clear the string:
        InputString = "";
        StringComplete = false;
        Comand = 0;
        Comand1 = 0;
        ActuatorValString = "";
        ActuatorValInt = 0;
        ActuatorValFloat = 0;
        for(int i = 0; i < 8; i++) {
        ActuatorValCharArray[i] = ' ';
        }
        break; 
        
      case 69:
        //Write 9th Element Calibration Y-inter (B) to EEPROM *DIYTempf5*
        calcfunction();
        EEPROM.writeFloat(148, ActuatorValFloat);  //Save value to EEPROM
        DIYTemp5CalB = ActuatorValFloat;
        Serial.print(ID);  //Group ID
        Serial.print("/");
        Serial.println("1"); 
                 // clear the string:
        InputString = "";
        StringComplete = false;
        Comand = 0;
        Comand1 = 0;
        ActuatorValString = "";
        ActuatorValInt = 0;
        ActuatorValFloat = 0;
        for(int i = 0; i < 8; i++) {
        ActuatorValCharArray[i] = ' ';
        }
        break; 

      case 70:
        //Write 11th Element Calibration Slope (M) to EEPROM *DIYTempf6* 
        calcfunction();
        EEPROM.writeFloat(176, ActuatorValFloat);  //Save value to EEPROM
        DIYTemp6CalM = ActuatorValFloat;
        Serial.print(ID);  //Group ID
        Serial.print("/");
        Serial.println("1"); 
        
                 // clear the string:
        InputString = "";
        StringComplete = false;
        Comand = 0;
        Comand1 = 0;
        ActuatorValString = "";
        ActuatorValInt = 0;
        ActuatorValFloat = 0;
        for(int i = 0; i < 8; i++) {
        ActuatorValCharArray[i] = ' ';
        }
        break; 
        
      case 71:
        //Write 11th Element Calibration Y-inter (B) to EEPROM *DIYTempf6*
        calcfunction();
        EEPROM.writeFloat(176, ActuatorValFloat);  //Save value to EEPROM
        DIYTemp6CalB = ActuatorValFloat;
        Serial.print(ID);  //Group ID
        Serial.print("/");
        Serial.println("1"); 
                 // clear the string:
        InputString = "";
        StringComplete = false;
        Comand = 0;
        Comand1 = 0;
        ActuatorValString = "";
        ActuatorValInt = 0;
        ActuatorValFloat = 0;
        for(int i = 0; i < 8; i++) {
        ActuatorValCharArray[i] = ' ';
        }
        break; 
        
      case 72:
        //Write 13th Element Calibration Slope (M) to EEPROM *DIYTempf7* 
        calcfunction();
        EEPROM.writeFloat(208, ActuatorValFloat);  //Save value to EEPROM
        DIYTemp7CalM = ActuatorValFloat;
        Serial.print(ID);  //Group ID
        Serial.print("/");
        Serial.println("1"); 
        
                 // clear the string:
        InputString = "";
        StringComplete = false;
        Comand = 0;
        Comand1 = 0;
        ActuatorValString = "";
        ActuatorValInt = 0;
        ActuatorValFloat = 0;
        for(int i = 0; i < 8; i++) {
        ActuatorValCharArray[i] = ' ';
        }
        break; 
        
      case 73:
        //Write 13th Element Calibration Y-inter (B) to EEPROM *DIYTempf7*
        calcfunction();
        EEPROM.writeFloat(212, ActuatorValFloat);  //Save value to EEPROM
        DIYTemp7CalB = ActuatorValFloat;
        Serial.print(ID);  //Group ID
        Serial.print("/");
        Serial.println("1"); 
                 // clear the string:
        InputString = "";
        StringComplete = false;
        Comand = 0;
        Comand1 = 0;
        ActuatorValString = "";
        ActuatorValInt = 0;
        ActuatorValFloat = 0;
        for(int i = 0; i < 8; i++) {
        ActuatorValCharArray[i] = ' ';
        }
        break; 
        
      case 74:
        //Write 15th Element Calibration Slope (M) to EEPROM *DIYTempf8* 
        calcfunction();
        EEPROM.writeFloat(240, ActuatorValFloat);  //Save value to EEPROM
        DIYTemp8CalM = ActuatorValFloat;
        Serial.print(ID);  //Group ID
        Serial.print("/");
        Serial.println("1"); 
        
                 // clear the string:
        InputString = "";
        StringComplete = false;
        Comand = 0;
        Comand1 = 0;
        ActuatorValString = "";
        ActuatorValInt = 0;
        ActuatorValFloat = 0;
        for(int i = 0; i < 8; i++) {
        ActuatorValCharArray[i] = ' ';
        }
        break; 
        
      case 75:
        //Write 15th Element Calibration Y-inter (B) to EEPROM *DIYTempf8*
        calcfunction();
        EEPROM.writeFloat(244, ActuatorValFloat);  //Save value to EEPROM
        DIYTemp8CalB = ActuatorValFloat;
        Serial.print(ID);  //Group ID
        Serial.print("/");
        Serial.println("1"); 
                 // clear the string:
        InputString = "";
        StringComplete = false;
        Comand = 0;
        Comand1 = 0;
        ActuatorValString = "";
        ActuatorValInt = 0;
        ActuatorValFloat = 0;
        for(int i = 0; i < 8; i++) {
        ActuatorValCharArray[i] = ' ';
        }
        break; 
      
      default:
        Serial.println("0");
         // clear the string:
        InputString = "";
        StringComplete = false;
        Comand = 0;
        Comand1 = 0;
        ActuatorValString = "";
        ActuatorValInt = 0;
        ActuatorValFloat = 0;
        for(int i = 0; i < 8; i++) {
        ActuatorValCharArray[i] = ' ';
        }
        break;   
      } 
    } 
    
    else {
      
      //Measure pH and case temp once a second, put them in an array
  TimeNow = millis();  //Check the time
  
   if (TimeNow > LastMeasurement + 1000) {
        LastMeasurement = millis();
  //Read Temp, add to strings, average, calibrate
      // subtract the last reading:
      DIYTemp1Total = DIYTemp1Total - DIYTemp1[index];
      DIYTemp2Total = DIYTemp2Total - DIYTemp2[index];
      DIYTemp3Total = DIYTemp3Total - DIYTemp3[index];
      DIYTemp4Total = DIYTemp4Total - DIYTemp4[index];
      DIYTemp5Total = DIYTemp5Total - DIYTemp5[index];
      DIYTemp6Total = DIYTemp6Total - DIYTemp6[index];
      DIYTemp7Total = DIYTemp7Total - DIYTemp7[index];
      DIYTemp8Total = DIYTemp8Total - DIYTemp8[index];
  
  // read from the sensor:  
      DIYTemp1[index] = analogRead(DIYTemp1Pin);  
      DIYTemp2[index] = analogRead(DIYTemp2Pin);
      DIYTemp3[index] = analogRead(DIYTemp3Pin);
      DIYTemp4[index] = analogRead(DIYTemp4Pin);
      DIYTemp5[index] = analogRead(DIYTemp5Pin);
      DIYTemp6[index] = analogRead(DIYTemp6Pin);
      DIYTemp7[index] = analogRead(DIYTemp7Pin);
      DIYTemp8[index] = analogRead(DIYTemp8Pin);
  
  // add the reading to the total:
      DIYTemp1Total = DIYTemp1Total + DIYTemp1[index];
      DIYTemp2Total = DIYTemp2Total + DIYTemp2[index];
      DIYTemp3Total = DIYTemp3Total + DIYTemp3[index];
      DIYTemp4Total = DIYTemp4Total + DIYTemp4[index];
      DIYTemp5Total = DIYTemp5Total + DIYTemp5[index];
      DIYTemp6Total = DIYTemp6Total + DIYTemp6[index];
      DIYTemp7Total = DIYTemp7Total + DIYTemp7[index];
      DIYTemp8Total = DIYTemp8Total + DIYTemp8[index];
  
  // calculate the average:        
      DIYTemp1Avg = DIYTemp1Total / numReadings;
      DIYTemp2Avg = DIYTemp2Total / numReadings;
      DIYTemp3Avg = DIYTemp3Total / numReadings;
      DIYTemp4Avg = DIYTemp4Total / numReadings;
      DIYTemp5Avg = DIYTemp5Total / numReadings;
      DIYTemp6Avg = DIYTemp6Total / numReadings;
      DIYTemp7Avg = DIYTemp7Total / numReadings;
      DIYTemp8Avg = DIYTemp8Total / numReadings;
  
  //Calibrate
      CalDIYTempf1 = DIYTemp1Avg + DIYTemp1CalB;     //Converting analog read into Farenheight
      CalDIYTempf1 = CalDIYTempf1 / DIYTemp1CalM;    // y=4.4138x+179.3788
      CalDIYTempc1 = CalDIYTempf1 - 32;      //Calculate
      CalDIYTempc1 = CalDIYTempc1 * 5 / 9 ;  //Celcius
      
      CalDIYTempf2 = DIYTemp2Avg + DIYTemp2CalB;     //Converting analog read into Farenheight
      CalDIYTempf2 = CalDIYTempf2 / DIYTemp2CalM;    //
      CalDIYTempc2 = CalDIYTempf2 - 32;      //Calculate
      CalDIYTempc2 = CalDIYTempc2 * 5 / 9 ;  //Celcius
      
      CalDIYTempf3 = DIYTemp3Avg + DIYTemp3CalB;     //Converting analog read into Farenheight
      CalDIYTempf3 = CalDIYTempf3 / DIYTemp3CalM;    //
      CalDIYTempc3 = CalDIYTempf3 - 32;      //Calculate
      CalDIYTempc3 = CalDIYTempc3 * 5 / 9 ;  //Celcius
      
      CalDIYTempf4 = DIYTemp4Avg + DIYTemp4CalB;     //Converting analog read into Farenheight
      CalDIYTempf4 = CalDIYTempf4 / DIYTemp4CalM;    //
      CalDIYTempc4 = CalDIYTempf4 - 32;      //Calculate
      CalDIYTempc4 = CalDIYTempc4 * 5 / 9 ;  //Celcius
      
      CalDIYTempf5 = DIYTemp5Avg + DIYTemp5CalB;     //Converting analog read into Farenheight
      CalDIYTempf5 = CalDIYTempf5 / DIYTemp5CalM;    //
      CalDIYTempc5 = CalDIYTempf5 - 32;      //Calculate
      CalDIYTempc5 = CalDIYTempc5 * 5 / 9 ;  //Celcius
      
      CalDIYTempf6 = DIYTemp6Avg + DIYTemp6CalB;     //Converting analog read into Farenheight
      CalDIYTempf6 = CalDIYTempf6 / DIYTemp6CalM;    //
      CalDIYTempc6 = CalDIYTempf6 - 32;      //Calculate
      CalDIYTempc6 = CalDIYTempc6 * 5 / 9 ;  //Celcius
      
      CalDIYTempf7 = DIYTemp7Avg + DIYTemp7CalB;     //Converting analog read into Farenheight
      CalDIYTempf7 = CalDIYTempf7 / DIYTemp7CalM;    //
      CalDIYTempc7 = CalDIYTempf7 - 32;      //Calculate
      CalDIYTempc7 = CalDIYTempc7 * 5 / 9 ;  //Celcius
      
      CalDIYTempf8 = DIYTemp8Avg + DIYTemp8CalB;     //Converting analog read into Farenheight
      CalDIYTempf8 = CalDIYTempf8 / DIYTemp8CalM;    //
      CalDIYTempc8 = CalDIYTempf8 - 32;      //Calculate
      CalDIYTempc8 = CalDIYTempc8 * 5 / 9 ;  //Celcius
   
  // advance to the next position in the array:  
      index = index + 1;                    

      // if we're at the end of the array...
      if (index >= numReadings)          {    
        // ...wrap around to the beginning:
        index = 0;      
      } 
    }    
      
      FloatSwitch1 = digitalRead(Float1Pin);
      FloatSwitch2 = digitalRead(Float2Pin);
      FloatSwitch3 = digitalRead(Float3Pin);
      FloatSwitch4 = digitalRead(Float4Pin);
      FloatSwitch5 = digitalRead(Float5Pin);
      FloatSwitch6 = digitalRead(Float6Pin);
      }
    
}
    
    
    
    
    
    
//Deals with incoming Serial Messages
void serialEvent() {
  String IDIn = "";
  while (Serial.available()) {
    char inChar = (char)Serial.read();   // get the new byte:
  //  Serial.print("inChar: ");
  //  Serial.println(inChar);  
    InputString += inChar;    // add it to the inputString:
 //   Serial.print("InputString: ");
 //   Serial.println(InputString);  
    if (inChar == '!') {     // if the incoming character is a newline, set a flag
      for (int i = 0; i<= 7; i++) {  //Read the ID
        char IDRead = InputString.charAt(i);
        IDIn += IDRead;
        
      }
      if (IDIn == ID) {   //If the IDs (IDs) Match
        if (SendData) {
          Pause = true;  //if it's sending continuous data, stop for a bit
        }
        StringComplete = true;    //Tell the main program there is input
        IDIn = "";
      } 
      else if ((InputString.charAt(0) == '0') && (InputString.length() == 2)) {
        //Serial.print("ID is: ");
        Serial.print(ID);  //Print out ID
        Serial.print("/");
        Serial.println(ID);
        IDIn = "";
        StringComplete = false;
        InputString = "";
      }
      else {
        Serial.println("-1");
      //  Serial.println("ID's dont match");
       // Serial.println(IDIn);
        IDIn = "";
        StringComplete = false;
        InputString = "";
      }
    }  
  }  
}


//Processes incoming string
void calcfunction() {
  String ActuatorValString = "";
  int StringLength = InputString.length();
  int x1 = InputString.charAt(9) - '0'; //converting char 9 (command #) to int
  
  if ((x1 >= 0) && (x1 <=9)) {  //If it is a 2 digit request
    for (int i = 11; i < StringLength; i++) {
      char ActuatorRead = InputString.charAt(i);
      ActuatorValString += ActuatorRead;
    }
  }
  else {   //If it is a 1 digit request
    for (int i = 10; i < StringLength; i++) {
      char ActuatorRead = InputString.charAt(i);
      ActuatorValString += ActuatorRead;
    }
  }
  
  ActuatorValString.toCharArray(ActuatorValCharArray, ActuatorValString.length());    //convert to Char array for EEPROM
  ActuatorValInt = ActuatorValString.toInt();  //An integer of the serial input
  ActuatorValFloat = ActuatorValString.toFloat();  //A float of the serial input
  //Serial.println(ActuatorValInt);
}

