# This Repo is no longer in use, all future updates can be found at: bitbucket.org/ReefPOM/ #
![Reef_Ai_Logo_Color.png](https://bitbucket.org/repo/EzEBBX/images/4285868125-Reef_Ai_Logo_Color.png)
 All code is Copyright: GNU General Public License V3
 All Schematics and PCB Files are Licensed: Creative Commons Attribution-ShareAlike 3.0 Unported License


**This means that you can use any of the code, schematics, or pcb's posted in this repository, however your versions must be licensed in the same way, and open/available to public view/use.*


### Arduino Code for the Reef Ai Periferals ###
* This is the code that is running on each of the sensors and periferals of the Reef Ai
* All the Arduino powered sensors communicate with a Python program running on the 'Brain'
   The Python software running on the Brain will be public on Git shortly.
* If you are interested in participating, contact: Staff@ReefAi.com
* Necessary Libraries are included in the source.  Clone the whole repository into your 'sketchbook' for easy use.
*  Check out the [Wiki](https://bitbucket.org/ReefAi/arduino-code/wiki/Home) for more details

### Contact Info:
*  Email: Staff@ReefAi.com
*  Website: www.ReefAi.com