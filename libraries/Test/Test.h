#ifndef Test_h  //Fixes issues if people double define Ocom
#define Test_h

#include "Arduino.h"
//#include <HardwareSerial.h>

typedef struct SensorElements {
  char SensorID[9] = "        ";  //ID  *when declaring a char array, you add 1 more than the number of chars, 
                                  // but for eeprom do not add the extra one.
  int SensorVal[10];  // the readings from the analog input as an array
  float SensorTotal = 0;       // the running total
  float SensorAvg = 0;    //The average
  float SensorCalVal = 0;    //Calibrated
  float SensorCalMVal = 0;    //Calibration Value M, Slope
  float SensorCalBVal = 0;    //Calibration Value B, Slope
  int SensorEEPROMIDLocation = 0;    //Location of the Element ID in EEPROM
  int SensorEEPROMCalMLocation = 0;  //Location of Calibration M in EEPROM
  int SensorEEPROMCalBLocation = 0;  //Location of Calibration B in EEPROM
  int SensorPin = 0;
  int SensorType = 0;
  };


class Test   //Wrap up all functions and variables of Ocom in this class
{
 // friend void serialEvent();  //Serial event seems to work well as a friend

  public:  //Functions and variables available to people
    Test();
    void success(void);
    void clearTheString(void);
    boolean stringComplete(void);
    int returnaint(void);
    String InputString;
    String inputstring;
  private:  //Functions and variables only available to the Ocom library
    boolean stringcomplete;
    boolean complete;
    int dogs;
    SensorElements SensorElementArray[];
};


#endif
