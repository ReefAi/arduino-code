/* CorePowerBar Ai Speak
 *
 * Copyright 2015 Scott Tomko, Greg Tomko, Linda Close
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * (GNU General Public License for more details.

 * You should have received a copy of the GNU Lesser General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * Contact:  Staff@ReefAi.com
 *
 *
 
Arduino Pinout: 
2 Triac #8
3 Triac #3
4 Triac #4
5 Triac #5
6 Triac #6
7 Triac #2
8 Triac #1
9 Triac #10
10 Triac #9
11 AC Phase Reading
14 Triac #7
20 Amp Reading (A6)
15 Temp
*/

const int PhasePin = 11;
String inputString = "";         // a string to hold incoming data
boolean stringComplete = false;  // whether the string is complete

int Comand = 0;
int Comand1 = 0;
int actuatorVal = 0;
unsigned long TimeNow = 0;
unsigned long StartTime = 0;
unsigned long LastSend = 0;
boolean SendData = false;
boolean SendQue = false;
int SendDelay = 0;
String ID = "gtp00001";  // <********** SET GROUP ID HERE *************
String CheckSumIn = "";
boolean Pause = false;

const int TriacPin1 = 8;
int Triac1 = 0;
const int TriacPin2 = 7;
int Triac2 = 0;
const int TriacPin3 = 3;
int Triac3= 0;
const int TriacPin4 = 4;
int Triac4 = 0;
const int TriacPin5 = 5;
int Triac5 = 0;
const int TriacPin6 = 6;
int Triac6 = 0;
const int TriacPin7 = 14;
int Triac7 = 0;
const int TriacPin8 = 2;
int Triac8 = 0;
const int TriacPin9 = 10;
int Triac9 = 0;
const int TriacPin10 = 9;
int Triac10 = 0;

const int AmpPin = A6;
int AmpAvg = 0;
int Amp = 0;
const int TempPin = 15;
int TempAvg = 0;
int Tempf = 0;
int Tempc = 0;

void setup() {
  Serial.begin(57600);
  // reserve 200 bytes for the inputString:
  inputString.reserve(200);
  
  pinMode(TriacPin1, OUTPUT);
  pinMode(TriacPin2, OUTPUT);
  pinMode(TriacPin3, OUTPUT);
  pinMode(TriacPin4, OUTPUT);
  pinMode(TriacPin5, OUTPUT)                 ;
  pinMode(TriacPin6, OUTPUT);
  pinMode(TriacPin7, OUTPUT);
  pinMode(TriacPin8, OUTPUT);
  pinMode(TriacPin9, OUTPUT);
  pinMode(TriacPin10, OUTPUT);
  
  pinMode(PhasePin, INPUT);
  pinMode(AmpPin, INPUT);
  pinMode(TempPin, INPUT);
}

// the loop routine runs over and over again forever:
void loop() {
   //See if it needs to send a continuous data stream for 60s
  TimeNow = millis();  //Check the time
  if ((StartTime + 60000 > TimeNow) && (SendQue)) { //do this for 1 minute 
    if (Pause) {
      SendData = false;
      Pause = false;
    }
    else {
      SendData = true;
    }
  }
  else {  //  Stop Sending Data
    SendData = false;
    SendQue = false;
    StartTime = 0;
    SendDelay = 0;
    LastSend = 0;
  }
  
  if (SendData) {  //Send Data at the specified Rate
    if (TimeNow > LastSend + SendDelay) {
          //Send Back Sensor Data to Ai
      Serial.print(ID);  // Group ID
        Serial.print("/");
        Serial.print("eam00000:");
        Serial.print(Amp);
        Serial.print(",");
        Serial.print("etf00010:");
        Serial.print(Tempf);
        Serial.print("etc00010:");
        Serial.println(Tempc);
      LastSend = millis();  //The program takes about 250ms to run
    }
  }  
  
  
if (stringComplete) {

    Comand = inputString.charAt(8) - '0';  //converting char to int
    Comand1 = inputString.charAt(9) - '0';  //converting char 2 to int
    if ((Comand1 >= 0) && (Comand1 <= 9) && (Comand1 != ':')) {
      Comand = 10;
      Comand += inputString.charAt(9) - '0';
    }
    
    switch (Comand) {
      case 0:
        //read device ID from flash memory
        Serial.println(ID);
        // clear the string:
        inputString = "";
        stringComplete = false;
        break;
      
      case 1:
         //Send Back Sensor Data
        Serial.print(ID);  //10 Power Bar
        Serial.print("Amps,");
        Serial.print(Amp);
        Serial.print(",");
        Serial.print("Temp,");
        Serial.println(Tempf);
        // clear the string:
        inputString = "";
        stringComplete = false;
        break;
      
      case 2:
       //Set Triac #1 from 0-100% on
       Triac1 = calcfunction(inputString);
        if (Triac1 == 0) {    //PWM pin #3&5 don't turn off all the way, this fixes that
          digitalWrite(TriacPin1, LOW);
          Serial.println("1");  
        }
        else {
        analogWrite(TriacPin1, Triac1);
        Serial.println("1"); 
        }
                        // clear the string:
        inputString = "";
        stringComplete = false; 
        break;
         
      case 3:
        //Set Triac #2 from 0-100% on
        Triac2 = calcfunction(inputString);
        analogWrite(TriacPin2, Triac2); 
                       // clear the string:
        inputString = "";
        stringComplete = false;  
        Serial.println("1");
        // clear the string:
        inputString = "";
        stringComplete = false;
        break;
      
      case 4:
         //Set Triac #3 on or off
        Triac3 = calcfunction(inputString);
        if (Triac3 == 0) {
            digitalWrite(TriacPin3, LOW);
            //Serial.print("Triac Set LOW");
            Serial.println("1");
            inputString = "";   // clear the string:
            stringComplete = false; 
            break;
        }
        
        else if (Triac3 == 100) {
            digitalWrite(TriacPin3, HIGH);
            Serial.println("1");
            inputString = "";   // clear the string:
            stringComplete = false; 
            break;
        }
            
        else {
            Serial.println("0");
            inputString = ""; // clear the string:
            stringComplete = false;  
            break;
        }        
  
      case 5:
        //Set Triac #4 on or off  
        Triac4 = calcfunction(inputString);
        if (Triac4 == 0) {
            digitalWrite(TriacPin4, LOW);
            Serial.println("1");
            inputString = "";   // clear the string:
            stringComplete = false; 
            break;
        }
        
        else if (Triac4 == 100) {
            digitalWrite(TriacPin4, HIGH);
            Serial.println("1");
            inputString = "";   // clear the string:
            stringComplete = false; 
            break;
        }
            
        else {
            Serial.println("0");
            inputString = ""; // clear the string:
            stringComplete = false;  
            break;
        }        
        
        case 10:
         //Send Back Sensor Data
        Serial.print(ID);  // Group ID
        Serial.print("/");
        Serial.print("eam00000:");
        Serial.print(Amp);
        Serial.print(",");
        Serial.print("etf00010:");
        Serial.println(Tempf);
        // clear the string:
        inputString = "";
        stringComplete = false;
        break;
        
        case 11:
          //Send Back Data for 60 Seconds
        SendDelay = calcfunction(inputString);  //Grab the time in milliseconds
        StartTime = millis();                   //to delay before sending again
        SendData = true;
        SendQue = true;
          // clear the string:
        inputString = "";
        stringComplete = false;
        Serial.println("1");
        break;
      
      default:
        Serial.println("0");
        // clear the string:
        inputString = "";
        stringComplete = false;  
        break;
    } 
  } 
    
    else {
      Amp = analogRead(AmpPin);
      Tempf = analogRead(TempPin);
      Tempf = Tempf - 179.3788;  //Convert to Farenheight 
      Tempf = Tempf / 4.4138; //y=4.4138x+179.3788
      Tempc = Tempf - 32;
      Tempc = Tempc * 5 / 9 ;
    }
 
}


    //Deals with incoming Serial Messages
void serialEvent() {
  while (Serial.available()) {
    char inChar = (char)Serial.read();   // get the new byte:
    inputString += inChar;    // add it to the inputString:
    if (inChar == '!') {     // if the incoming character is a newline, set a flag
      for (int i = 0; i<= 7; i++) {  //Read the Checksum
        char CheckSumRead = inputString.charAt(i);
        CheckSumIn += CheckSumRead;
      }
      if (CheckSumIn == ID) {   //If the CheckSums (IDs) Match
        if (SendData) {
          Pause = true;  //if it's sending continuous data, stop for a bit
        }
        stringComplete = true;    //Tell the main program there is input
        CheckSumIn = "";
      }   //int z = inputString.charAt(0) - '0'; //converting char 2 to int
      else if ((inputString.charAt(0) == '0') && (inputString.length() == 2)) {
        Serial.println(ID);
        CheckSumIn = "";
        stringComplete = false;
        inputString = "";
      }
      else {
        Serial.println("-1");
        CheckSumIn = "";
        stringComplete = false;
        inputString = "";
      }
    }
  }
}

    //Processes incoming string
int calcfunction(String inString) {
  String actuatorValString = "";
  int StringLength = inString.length();
  int x1 = inputString.charAt(9) - '0'; //converting char 2 to int
  
  if ((x1 >= 0) && (x1 <=9)) {  //If it is a 2 digit request
    for (int i = 11; i <= StringLength; i++) {
      char actuatorRead = inString.charAt(i);
      actuatorValString += actuatorRead;
    }
  }
  else {   //If it is a 1 digit request
    for (int i = 10; i <= StringLength; i++) {
      char actuatorRead = inString.charAt(i);
      actuatorValString += actuatorRead;
    }
  }
  actuatorVal = actuatorValString.toInt();
  
//  Serial.print("Actuator Val in Subprogram ");
//  Serial.println(actuatorVal);
  return actuatorVal;
}
