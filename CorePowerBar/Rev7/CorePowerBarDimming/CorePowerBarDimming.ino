/* CorePowerBar Ai Speak Dimming Rev.7
 *
 * Copyright 2015 Scott Tomko, Greg Tomko, Linda Close
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * (GNU General Public License for more details.

 * You should have received a copy of the GNU Lesser General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * Contact:  Staff@ReefAi.com
 *
 *
 
Arduino Pinout: 
2 AC Phase Input
11 Traic #1 (new)
3 Triac #2
4 Triac #3
5 0-10v #1
6 0-10v #4
7 Triac #4
8 Bottom LED
9 0-10v #2
10 0-10v #3
ADC6 Thermisor (new)
ADC7 pH Reading
12  DIY Pin #3
13  DIY Pin #4
14 Right LED
15 Center LED
16 Left LED
17 Top LED
18 DIY Pin #2 (SDA)
19 DIY Pin #1 (SCL)
*/

#include <EEPROMex.h>  //For Calibration Data and ID


//Variables for testing

//Variable needed by all Ai Arduino's:
String InputString = "";
boolean StringComplete = false;
boolean Pause = false;
int Comand = 0;
int Comand1 = 0;
unsigned long WatchDogTime = 0;
int WatchDogEnable = 0;
unsigned long TimeNow = 0;
unsigned long StartTime = 0;
unsigned long LastSend = 0;
unsigned long LastMeasurement = 0;
boolean SendData = false;
boolean SendQue = false;
int SendDelay = 0;
char ID[] = "        ";
int ActuatorValInt = 0;   //Processed String Data in 3 data types:
float ActuatorValFloat = 0;
String ActuatorValString = "";
char ActuatorValCharArray[] = "        ";
char LEDBID[] = "        ";
int LEDB = 0;

//Analog and Digital Reading Section
const int numReadings = 30;  //number of analog readings in array
int index = 0;        // the index of the current reading
int PCBTemp[numReadings];  // the readings from the analog input as an array
float PCBTempTotal = 0;    // the running total
float PCBTempAvg = 0;    //The average
float CalPCBTempc = 0;  //Calibrated
float CalPCBTempf = 0;
char PCBTempfID[] = "        ";
char PCBTempcID[] = "        ";
int pH[numReadings];  // the readings from the analog input as an array
float pHTotal = 0;    // the running total
float pHAvg = 0;    //The average
float CalpH = 0;  //Calibrated
char pHID[] = "        ";
int DIY1[numReadings];  // the readings from the analog input as an array
float DIY1Total = 0;    // the running total
float DIY1Avg = 0;    //The average
float CalDIY1 = 0;  //Calibrated
char DIY1ID[] = "        ";
int DIY2[numReadings];  // the readings from the analog input as an array
float DIY2Total = 0;    // the running total
float DIY2Avg = 0;    //The average
float CalDIY2 = 0;  //Calibrated
char DIY2ID[] = "        ";
int DIY3[numReadings];  // the readings from the analog input as an array
float DIY3Total = 0;    // the running total
float DIY3Avg = 0;    //The average
float CalDIY3 = 0;  //Calibrated
char DIY3ID[] = "        ";
int DIY4[numReadings];  // the readings from the analog input as an array
float DIY4Total = 0;    // the running total
float DIY4Avg = 0;    //The average
float CalDIY4 = 0;  //Calibrated
char DIY4ID[] = "        ";

char ZTten1ID[] = "        ";
char ZTten2ID[] = "        ";
char ZTten3ID[] = "        ";
char ZTten4ID[] = "        ";
char Triac1ID[] = "        ";
char Triac2ID[] = "        ";
char Triac3ID[] = "        ";
char Triac4ID[] = "        ";

//calibration variables
float PCBTempCalM = 0;  
float PCBTempCalB = 0;
float pHCalM = 0;
float pHCalB = 0;
float DIY1CalM = 0;
float DIY1CalB = 0;
float DIY2CalM = 0;
float DIY2CalB = 0;
float DIY3CalM = 0;
float DIY3CalB = 0;
float DIY4CalM = 0;
float DIY4CalB = 0;

//Triac Dimming Section
volatile int ZeroCross = 0;
volatile unsigned long ZeroCrossTime = 0;  //volatile variables are stored in ram, and function faster (for dimming)
volatile unsigned long LastCrossTime = 0;
volatile unsigned long LastTriac1Pulse = 0;
volatile unsigned long LastTriac2Pulse = 0;
volatile unsigned long LastTriac3Pulse = 0;
volatile unsigned long LastTriac4Pulse = 0;
volatile unsigned long TimeNowMicro = 0;
boolean Dim1 = false;
boolean Dim2 = false;
boolean Dim3 = false;
boolean Dim4 = false;
int Triac1Delay = 0;
int Triac1DelayMicro = 0;
int Triac1FS = 0; //Fail Safe Value Saved in Micro's in EEPROM
int Triac2Delay = 0;
int Triac2DelayMicro = 0;
int Triac2FS = 0; //Fail Safe Value Saved in Micro's in EEPROM
int Triac3Delay = 0;
int Triac3DelayMicro = 0;
int Triac3FS = 0; //Fail Safe Value Saved in Micro's in EEPROM
int Triac4Delay = 0;
int Triac4DelayMicro = 0;
int Triac4FS = 0; //Fail Safe Value Saved in Micro's in EEPROM


//Declaring Arduino Pins
const int PhasePin = 2;
const int pHPin = A7;
const int PCBTempPin = A6;
const int TopLEDPin = 17;
const int BottomLEDPin = 8;
const int LeftLEDPin = 16;
const int RightLEDPin = 14;
const int CenterLEDPin = 15;
const int TriacPin1 = 11;
const int TriacPin2 = 3;
const int TriacPin3 = 4;
const int TriacPin4 = 7;
const int ZTtenPin1 = 5;
const int ZTtenPin2 = 9;
const int ZTtenPin3 = 10;
const int ZTtenPin4 = 6;
const int DIYPin1 = 19;  //SCL
const int DIYPin2 = 18;  //SDA
const int DIYPin3 = 12;  //MISO
const int DIYPin4 = 13;  //SCK

//Zero to 10 volt variables
int ZTten1 = 0;
int ZTten2 = 0;
int ZTten3 = 0;
int ZTten4 = 0;
int ZTten1FS = 0;  //Fail Safes
int ZTten2FS = 0;
int ZTten3FS = 0;
int ZTten4FS = 0;

void setup() {
  Serial.begin(57600);
  // reserve 200 bytes for the inputString:
  InputString.reserve(200);

   // initialize all the analog readings to 0:
  for (int thisReading = 0; thisReading < numReadings; thisReading++) {
    PCBTemp[thisReading] = 0;  
    pH[thisReading] = 0;  
    DIY1[thisReading] = 0;  
    DIY2[thisReading] = 0;
    DIY3[thisReading] = 0;  
    DIY4[thisReading] = 0;
  }

  pinMode(TopLEDPin, OUTPUT);
  pinMode(BottomLEDPin, OUTPUT);
  pinMode(LeftLEDPin, OUTPUT);
  pinMode(RightLEDPin, OUTPUT);
  pinMode(CenterLEDPin, OUTPUT);

  pinMode(ZTtenPin1, OUTPUT);
  pinMode(ZTtenPin2, OUTPUT);
  pinMode(ZTtenPin3, OUTPUT);
  pinMode(ZTtenPin4, OUTPUT); 

  pinMode(TriacPin1, OUTPUT);
  pinMode(TriacPin2, OUTPUT);
  pinMode(TriacPin3, OUTPUT);
  pinMode(TriacPin4, OUTPUT);
  //Set all other pins as Input so they are safe

  pinMode(PhasePin, INPUT);
  pinMode(pHPin, INPUT);
  pinMode(PCBTempPin, INPUT);
  pinMode(DIYPin1, INPUT);
  pinMode(DIYPin2, INPUT);
  pinMode(DIYPin3, INPUT);
  pinMode(DIYPin4, INPUT);      
  
//Flash the LED's
  digitalWrite(TopLEDPin, HIGH);
  digitalWrite(BottomLEDPin, HIGH);
  digitalWrite(LeftLEDPin, HIGH);
  digitalWrite(RightLEDPin, HIGH);
  digitalWrite(CenterLEDPin, HIGH);
  delay(2000);
  digitalWrite(TopLEDPin, LOW);
  digitalWrite(BottomLEDPin, LOW);
  digitalWrite(LeftLEDPin, LOW);
  digitalWrite(RightLEDPin, LOW);
  digitalWrite(CenterLEDPin, LOW);

  WatchDogTime = millis();  //Delay the watchdog
  
  EEPROM.readBlock<char>(216, ID, 8);   //Read In Element ID's from EEPROM
  EEPROM.readBlock<char>(8, PCBTempfID, 8); 
  EEPROM.readBlock<char>(24, PCBTempcID, 8);
  EEPROM.readBlock<char>(40, pHID, 8);
  EEPROM.readBlock<char>(56, DIY1ID, 8);
  EEPROM.readBlock<char>(72, DIY2ID, 8);
  EEPROM.readBlock<char>(88, DIY3ID, 8);
  EEPROM.readBlock<char>(104, DIY4ID, 8);
  
  EEPROM.readBlock<char>(120, LEDBID, 8);
  EEPROM.readBlock<char>(128, ZTten1ID, 8);
  EEPROM.readBlock<char>(136, ZTten2ID, 8);
  EEPROM.readBlock<char>(144, ZTten3ID, 8);
  EEPROM.readBlock<char>(152, ZTten4ID, 8);
  EEPROM.readBlock<char>(160, Triac1ID, 8);
  EEPROM.readBlock<char>(168, Triac2ID, 8);
  EEPROM.readBlock<char>(176, Triac3ID, 8);
  EEPROM.readBlock<char>(184, Triac4ID, 8);
  
  
  LEDB = EEPROM.readInt(192);  //Read LED brightness
  ZTten1FS = EEPROM.readInt(194);
  ZTten2FS = EEPROM.readInt(196);
  ZTten3FS = EEPROM.readInt(198);
  ZTten4FS = EEPROM.readInt(200);
  Triac1FS = EEPROM.readInt(202);
  Triac2FS = EEPROM.readInt(204);
  Triac3FS = EEPROM.readInt(206);
  Triac4FS = EEPROM.readInt(208);
  
  WatchDogEnable = EEPROM.readInt(210);  //Watchdog Enable/Diable
 
  PCBTempCalM = EEPROM.readFloat(16);
  PCBTempCalB = EEPROM.readFloat(20);
  pHCalM = EEPROM.readFloat(48);
  pHCalB = EEPROM.readFloat(52);
  DIY1CalM = EEPROM.readFloat(64);
  DIY1CalB = EEPROM.readFloat(68);
  DIY2CalM = EEPROM.readFloat(80);
  DIY2CalB = EEPROM.readFloat(84);
  DIY3CalM = EEPROM.readFloat(96);
  DIY3CalB = EEPROM.readFloat(100);
  DIY4CalM = EEPROM.readFloat(112);
  DIY4CalB = EEPROM.readFloat(116);

//Assign a group ID of 00000000 if it doesn't have one
      char a = EEPROM.read(216);
      //Serial.println(a);
      if (a != 'g') {
        EEPROM.writeBlock<char>(216,"00000000", 8);
    //    Serial.println("writing 0's to EEPROM");
        EEPROM.readBlock<char>(216, ID, 8);
        Serial.println(ID);
       }
//Ask for Actuator Data at Startup
  Serial.print(ID);
  Serial.print("/");
  Serial.println("SendActData");
}

// the loop routine runs over and over again forever:
void loop() {

//Dimming outlet controll section
  ZeroCross = digitalRead(PhasePin);  //Check for a Zero Cross
  TimeNowMicro = micros();  //Read the time right now
  if (ZeroCross) {
    ZeroCrossTime = TimeNowMicro;
  }
  if ((ZeroCross) && (ZeroCrossTime > LastCrossTime + 7000)) {  //avoids repeat ZeroCross measurements
    LastCrossTime = ZeroCrossTime;                              //There are 8300micro seconds in half a sign wave @ 60Hz
  }
  if ((Dim1) && (TimeNowMicro > LastCrossTime + Triac1DelayMicro) && (TimeNowMicro < LastCrossTime + 8000) && (TimeNowMicro > LastTriac1Pulse + 8000 - Triac1DelayMicro)) {
    digitalWrite(TriacPin1, HIGH);   //Turn on the Triac if it's time, and you havent already just done it
    LastTriac1Pulse = TimeNowMicro;
  }
  if ((Dim2) && (TimeNowMicro > LastCrossTime + Triac2DelayMicro) && (TimeNowMicro < LastCrossTime + 8000) && (TimeNowMicro > LastTriac2Pulse + 8000 - Triac2DelayMicro)) {
    digitalWrite(TriacPin2, HIGH);  //Turn on the Triac if it's time, and you havent already just done it
    LastTriac2Pulse = TimeNowMicro;
    //Serial.println("Triac 2 On");
  }
  if ((Dim3) && (TimeNowMicro > LastCrossTime + Triac3DelayMicro) && (TimeNowMicro < LastCrossTime + 8000) && (TimeNowMicro > LastTriac3Pulse + 8000 - Triac3DelayMicro)) {
    digitalWrite(TriacPin3, HIGH);  //Turn on the Triac if it's time, and you havent already just done it
    LastTriac3Pulse = TimeNowMicro;
  }
  if ((Dim4) && (TimeNowMicro > LastCrossTime + Triac4DelayMicro) && (TimeNowMicro < LastCrossTime + 8000) && (TimeNowMicro > LastTriac4Pulse + 8000 - Triac4DelayMicro)) { 
    digitalWrite(TriacPin4, HIGH);  //Turn on the Triac if it's time, and you havent already just done it
    LastTriac4Pulse = TimeNowMicro;
  }
 
    //Turn off Triacs so they don't stick on at zero cross
  if (TimeNowMicro > LastCrossTime + 7000) {
    digitalWrite(TriacPin1, LOW);
    digitalWrite(TriacPin2, LOW);
    digitalWrite(TriacPin3, LOW);
    digitalWrite(TriacPin4, LOW);
  } 

  
//Measure pH and case temp once a second, put them in an array
  TimeNow = millis();  //Check the time
  
   if (TimeNow > LastMeasurement + 1000) {
        LastMeasurement = millis();
  //Read Temp, pH, DIYPins, add to strings, average, calibrate
      // subtract the last reading:
      PCBTempTotal = PCBTempTotal - PCBTemp[index];        
      pHTotal = pHTotal - pH[index];
      DIY1Total = DIY1Total - DIY1[index];
      DIY2Total = DIY2Total - DIY2[index];
      DIY3Total = DIY3Total - DIY3[index];
      DIY4Total = DIY4Total - DIY4[index];
  
  // read from the sensor:  
      PCBTemp[index] = analogRead(PCBTempPin);
      pH[index] = analogRead(pHPin);
      DIY1[index] = analogRead(DIYPin1);  
      DIY2[index] = analogRead(DIYPin2); 
      DIY3[index] = analogRead(DIYPin3); 
      DIY4[index] = analogRead(DIYPin4); 
  
  // add the reading to the total:
      PCBTempTotal= PCBTempTotal + PCBTemp[index];
      pHTotal = pHTotal + pH[index];
      DIY1Total = DIY1Total + DIY1[index];
      DIY2Total = DIY2Total + DIY2[index];
      DIY3Total = DIY3Total + DIY3[index];
      DIY4Total = DIY4Total + DIY4[index];
  
  // calculate the average:        
      PCBTempAvg = PCBTempTotal / numReadings;
      pHAvg = pHTotal / numReadings;
      DIY1Avg = DIY1Total / numReadings;
      DIY2Avg = DIY2Total / numReadings;
      DIY3Avg = DIY3Total / numReadings;
      DIY4Avg = DIY4Total / numReadings;
  
  //Calibrate
      CalPCBTempf = PCBTempAvg - PCBTempCalB;  //Converting analog read into Farenheight
      CalPCBTempf = CalPCBTempf / PCBTempCalM;  // y=4.4138x+179.3788
      CalPCBTempc = CalPCBTempf - 32;      //Calculate
      CalPCBTempc = CalPCBTempc * 5 / 9 ;  //Celcius
      CalpH = pHAvg - pHCalB;  //Calibrating pH
      CalpH = CalpH / pHCalM;  //y=117x-362
      CalDIY1 = DIY1Avg + DIY1CalB;
      CalDIY1 = CalDIY1 / DIY1CalM;
      CalDIY2 = DIY2Avg + DIY2CalB;
      CalDIY2 = CalDIY2 / DIY2CalM;
      CalDIY3 = DIY3Avg + DIY3CalB;
      CalDIY3 = CalDIY3 / DIY3CalM;
      CalDIY4 = DIY4Avg + DIY4CalB;
      CalDIY4 = CalDIY4 / DIY4CalM;
   
  // advance to the next position in the array:  
      index = index + 1;                    

      // if we're at the end of the array...
      if (index >= numReadings)          {    
        // ...wrap around to the beginning:
        index = 0;  
        //Calculate Level         
      } 
    }    
  
  

//See if it needs to send a continuous data stream for 60s, and do so
  if ((StartTime + 60000 > TimeNow) && (SendQue)) { //do this for 1 minute 
    if (Pause) {
      SendData = false;
      Pause = false;
    }
    else {
      SendData = true;
    }
  }
  else {  //  Stop Sending Data
    SendData = false;
    SendQue = false;
    StartTime = 0;
    SendDelay = 0;
    LastSend = 0;
  }
  
  if (SendData) {  //Send Data at the specified Rate
    if (TimeNow > LastSend + SendDelay) {
          //Send Back Sensor Data to Ai
      Serial.print(ID);  // Group ID
        Serial.print("/");
        Serial.print(pHID);
        Serial.print(":");
        Serial.print(CalpH);
        Serial.print(",");
        Serial.print(PCBTempfID);
        Serial.print(":");
        Serial.print(CalPCBTempf);
        Serial.print(",");
        Serial.print(PCBTempcID);
        Serial.print(":");
        Serial.print(CalPCBTempc);
        Serial.print(",");
        Serial.print(DIY1ID);
        Serial.print(":");
        Serial.print(CalDIY1);
        Serial.print(",");
        Serial.print(DIY2ID);
        Serial.print(":");
        Serial.print(CalDIY2);
        Serial.print(",");
        Serial.print(DIY3ID);
        Serial.print(":");
        Serial.print(CalDIY3);
        Serial.print(",");
        Serial.print(DIY4ID);
        Serial.print(":");
        Serial.println(CalDIY4);
      LastSend = millis();
    }
  }  


if (WatchDogEnable == 1) {
    if (TimeNow > WatchDogTime + 300000) {  //If the arduino hasn't recieved any signal in 5 minutes
        //Initiate Fail-States
      //Triac1 = Triac1FS  
      //blink the leds
      }
    }    
  
//Decides what to do when it recieves a serial command
  if (StringComplete) {
    WatchDogTime = millis();  //Check the time
    Comand = InputString.charAt(8) - '0';  //converting char to int
    Comand1 = InputString.charAt(9) - '0';  //converting char 2 to int
    if ((Comand1 >= 0) && (Comand1 <= 9) && (Comand1 != ':')) {
      Comand = Comand * 10;
      Comand += Comand1;
    } 
    switch (Comand) {
      case 0:
        //read device ID from flash memory
        Serial.print(ID);  // Group ID
        Serial.print("/");
        Serial.println(ID);
                 // clear the string:
        InputString = "";
        StringComplete = false;
        Comand = 0;
        Comand1 = 0;
        ActuatorValString = "";
        ActuatorValInt = 0;
        ActuatorValFloat = 0;
        for(int i = 0; i < 8; i++) {
        ActuatorValCharArray[i] = ' ';
        }
        break;  
      
      case 1:
         //Send Back Sensor Data
        Serial.print(ID);  // Group ID
        Serial.print("/");
        Serial.print(pHID);
        Serial.print(":");
        Serial.print(analogRead(pHPin));
        Serial.print(",");
        Serial.print(PCBTempfID);
        Serial.print(":");
        Serial.println(analogRead(PCBTempPin));
      //  Serial.print(",");
      //  Serial.print("Tempc,");
      //  Serial.println(Tempc);
                 // clear the string:
        InputString = "";
        StringComplete = false;
        Comand = 0;
        Comand1 = 0;
        ActuatorValString = "";
        ActuatorValInt = 0;
        ActuatorValFloat = 0;
        for(int i = 0; i < 8; i++) {
        ActuatorValCharArray[i] = ' ';
        }
        break;   

      case 2:
        //Set 0-10v #1 pin
        calcfunction();
        ZTten1 = ActuatorValInt;
        ZTten1 = ZTten1 * 255 / 100;  //convert to 0-255 for analog write
        if (ZTten1 == 0) {    //PWM pin #3&5 don't turn off all the way, this fixes that
          digitalWrite(ZTtenPin1, LOW);
          Serial.print(ID);  //Group ID
          Serial.print("/");
          Serial.println("1");
        }
        else {
          analogWrite(ZTtenPin1, ZTten1);
          Serial.print(ID);  //Group ID
          Serial.print("/");
          Serial.println("1"); 
        }
                 // clear the string:
        InputString = "";
        StringComplete = false;
        Comand = 0;
        Comand1 = 0;
        ActuatorValString = "";
        ActuatorValInt = 0;
        ActuatorValFloat = 0;
        for(int i = 0; i < 8; i++) {
        ActuatorValCharArray[i] = ' ';
        }
        break;  

      case 3:
        //Set 0-10v #2 pin
        calcfunction();
        ZTten2 = ActuatorValInt;
        ZTten2 = ZTten2 * 255 / 100;  //convert to 0-255 for analog write
        analogWrite(ZTtenPin2, ZTten2);
        Serial.print(ID);  //Group ID
        Serial.print("/");
        Serial.println("1"); 
                 // clear the string:
        InputString = "";
        StringComplete = false;
        Comand = 0;
        Comand1 = 0;
        ActuatorValString = "";
        ActuatorValInt = 0;
        ActuatorValFloat = 0;
        for(int i = 0; i < 8; i++) {
        ActuatorValCharArray[i] = ' ';
        }
        break;  

      case 4:
        //Set 0-10v #3 pin  
        calcfunction();
        ZTten3 = ActuatorValInt;
        ZTten3 = ZTten3 * 255 / 100;  //convert to 0-255 for analog write
        analogWrite(ZTtenPin3, ZTten3);
        Serial.print(ID);  //Group ID
        Serial.print("/");
        Serial.println("1"); 
                 // clear the string:
        InputString = "";
        StringComplete = false;
        Comand = 0;
        Comand1 = 0;
        ActuatorValString = "";
        ActuatorValInt = 0;
        ActuatorValFloat = 0;
        for(int i = 0; i < 8; i++) {
        ActuatorValCharArray[i] = ' ';
        }
        break; 

      case 5:
        //Set 0-10v #4 pin   
        calcfunction(); 
        ZTten4 = ActuatorValInt;
        ZTten4 = ZTten4 * 255 / 100;  //convert to 0-255 for analog write
        analogWrite(ZTtenPin4, ZTten4);
        Serial.print(ID);  //Group ID
        Serial.print("/");
        Serial.println("1"); 
                 // clear the string:
        InputString = "";
        StringComplete = false;
        Comand = 0;
        Comand1 = 0;
        ActuatorValString = "";
        ActuatorValInt = 0;
        ActuatorValFloat = 0;
        for(int i = 0; i < 8; i++) {
        ActuatorValCharArray[i] = ' ';
        }
        break; 

      case 6:
        //Set Triac #1 ON, OFF, or Dimmed
        calcfunction();
        Triac1Delay = ActuatorValInt;
        if (Triac1Delay == 0) {    //If Off, set Off normally
          digitalWrite(TriacPin4, LOW);
          Triac1DelayMicro = 8000;
          Serial.print(ID);  //Group ID
          Serial.print("/");
          Serial.println("1"); 
          Dim1 = false;  
        }
        else if (Triac1Delay == 100) {  //If On, ser On normally
          digitalWrite(TriacPin1, HIGH);
          Serial.print(ID);  //Group ID
          Serial.print("/");
          Serial.println("1"); 
          Triac1DelayMicro = 0;
          Dim1 = true;
        }
        else if ((Triac1Delay < 100) && (Triac1Delay > 0)) {  
          Triac1DelayMicro = 80 * Triac1Delay;  //convert to Delay:
          Triac1DelayMicro = 8000 - Triac1DelayMicro;  // MilliDelay=8300-(83*%Dim)
          Dim1 = true;
          Serial.print(ID);  //Group ID
          Serial.print("/");
          Serial.println("1"); 
        }
        else {
          Serial.print(ID);  //Group ID
          Serial.print("/");
          Serial.println("0"); 
        }
                 // clear the string:
        InputString = "";
        StringComplete = false;
        Comand = 0;
        Comand1 = 0;
        ActuatorValString = "";
        ActuatorValInt = 0;
        ActuatorValFloat = 0;
        for(int i = 0; i < 8; i++) {
        ActuatorValCharArray[i] = ' ';
        }
        break;   

      case 7:
        //Set Triac #2 ON, OFF, or Dimmed
        calcfunction();
        Triac2Delay = ActuatorValInt;
        if (Triac2Delay == 0) {    //If Off, set Off normally
          digitalWrite(TriacPin2, LOW);
          Triac2DelayMicro = 8000;
          Serial.print(ID);  //Group ID
          Serial.print("/");
          Serial.println("1"); 
          Dim2 = false;  
        }
        else if (Triac2Delay == 100) {  //If On, ser On normally
          digitalWrite(TriacPin2, HIGH);
          Serial.print(ID);  //Group ID
          Serial.print("/");
          Serial.println("1");  
          Triac2DelayMicro = 0;
          Dim2 = true;
        }
        else if ((Triac2Delay < 100) && (Triac2Delay > 0)) {  
          Triac2DelayMicro = 80 * Triac2Delay;  //convert to Delay:
          Triac2DelayMicro = 8000 - Triac2DelayMicro;  // MilliDelay=8300-(83*%Dim)
          Dim2 = true;
          Serial.print(ID);  //Group ID
          Serial.print("/");
          Serial.println("1");  
        }
        else {
          Serial.print(ID);  //Group ID
          Serial.print("/");
          Serial.println("0"); 
        }
                 // clear the string:
        InputString = "";
        StringComplete = false;
        Comand = 0;
        Comand1 = 0;
        ActuatorValString = "";
        ActuatorValInt = 0;
        ActuatorValFloat = 0;
        for(int i = 0; i < 8; i++) {
        ActuatorValCharArray[i] = ' ';
        }
        break; 

      case 8:
        //Set Triac #3 ON, OFF, or Dimmed
        calcfunction();
        Triac3Delay = ActuatorValInt;
        if (Triac3Delay == 0) {    //If Off, set Off normally
          digitalWrite(TriacPin3, LOW);
          Triac3DelayMicro = 8000;  //*******ERROR WAS HERE**********was: Triac4delaymicro*********
          Serial.print(ID);  //Group ID
          Serial.print("/");
          Serial.println("1"); 
          Dim3 = false;     ///**********ERROR WAS HERE************was: Dim4= false********************
        }
        else if (Triac3Delay == 100) {  //If On, ser On normally
          digitalWrite(TriacPin3, HIGH);
          Serial.print(ID);  //Group ID
          Serial.print("/");
          Serial.println("1");  
          Triac3DelayMicro = 0;
          Dim3 = true;
        }
        else if ((Triac3Delay < 100) && (Triac3Delay > 0)) {  
          Triac3DelayMicro = 80 * Triac3Delay;  //convert to Delay:
          Triac3DelayMicro = 8000 - Triac3DelayMicro;  // MilliDelay=8300-(83*%Dim)
          Dim3 = true;
          Serial.print(ID);  //Group ID
          Serial.print("/");
          Serial.println("1");  
        }
        else {
          Serial.print(ID);  //Group ID
          Serial.print("/");
          Serial.println("0"); 
        }
                 // clear the string:
        InputString = "";
        StringComplete = false;
        Comand = 0;
        Comand1 = 0;
        ActuatorValString = "";
        ActuatorValInt = 0;
        ActuatorValFloat = 0;
        for(int i = 0; i < 8; i++) {
        ActuatorValCharArray[i] = ' ';
        }
        break; 

      case 9:
        //Set Triac #4 ON, OFF, or Dimmed
        calcfunction();
        Triac4Delay = ActuatorValInt;
        if (Triac4Delay == 0) {    //If Off, set Off normally
          digitalWrite(TriacPin4, LOW);
          Triac4DelayMicro = 8000;
          Serial.print(ID);  //Group ID
          Serial.print("/");
          Serial.println("1"); 
          Dim4 = false;  
        }
        else if (Triac4Delay == 100) {  //If On, set On normally
          digitalWrite(TriacPin4, HIGH);
          Serial.print(ID);  //Group ID
          Serial.print("/");
          Serial.println("1"); 
          Triac4DelayMicro = 0;
          Dim4 = true;
        }
        else if ((Triac4Delay < 100) && (Triac4Delay > 0)) {  
          Triac4DelayMicro = 80 * Triac4Delay;  //convert to Delay:
          Triac4DelayMicro = 8000 - Triac4DelayMicro;  // MilliDelay=8300-(83*%Dim)
          Dim4 = true;
          Serial.print(ID);  //Group ID
          Serial.print("/");
          Serial.println("1"); 
        }
        else {
          Serial.print(ID);  //Group ID
          Serial.print("/");
          Serial.println("0"); 
        }
                 // clear the string:
        InputString = "";
        StringComplete = false;
        Comand = 0;
        Comand1 = 0;
        ActuatorValString = "";
        ActuatorValInt = 0;
        ActuatorValFloat = 0;
        for(int i = 0; i < 8; i++) {
        ActuatorValCharArray[i] = ' ';
        }
        break; 
       
      case 10:
        Serial.print(ID);  // Group ID
        Serial.print("/");
        Serial.print(pHID);
        Serial.print(":");
        Serial.print(CalpH);
        Serial.print(",");
        Serial.print(PCBTempfID);
        Serial.print(":");
        Serial.print(CalPCBTempf);
        Serial.print(",");
        Serial.print(PCBTempcID);
        Serial.print(":");
        Serial.print(CalPCBTempc);
        Serial.print(",");
        Serial.print(DIY1ID);
        Serial.print(":");
        Serial.print(CalDIY1);
        Serial.print(",");
        Serial.print(DIY2ID);
        Serial.print(":");
        Serial.print(CalDIY2);
        Serial.print(",");
        Serial.print(DIY3ID);
        Serial.print(":");
        Serial.print(CalDIY3);
        Serial.print(",");
        Serial.print(DIY4ID);
        Serial.print(":");
        Serial.println(CalDIY4);
                 // clear the string:
        InputString = "";
        StringComplete = false;
        Comand = 0;
        Comand1 = 0;
        ActuatorValString = "";
        ActuatorValInt = 0;
        ActuatorValFloat = 0;
        for(int i = 0; i < 8; i++) {
        ActuatorValCharArray[i] = ' ';
        }
        break;  
        
      case 11:
          //Send Back Data for 60 Seconds
        calcfunction();
        SendDelay = ActuatorValInt;  //Grab the time in milliseconds
        StartTime = millis();                   //to delay before sending again
        SendData = true;
        SendQue = true;
                   // clear the string:
        InputString = "";
        StringComplete = false;
        Comand = 0;
        Comand1 = 0;
        ActuatorValString = "";
        ActuatorValInt = 0;
        ActuatorValFloat = 0;
        for(int i = 0; i < 8; i++) {
        ActuatorValCharArray[i] = ' ';
        }
        break; 

      case 13:
          //Send Back Actuator Data
        Serial.print(ID);  //Group ID
        Serial.print("/");
        Serial.print(LEDBID);
        Serial.print(":");
        Serial.print(LEDB);
        Serial.print(",");
        Serial.print(Triac1ID);
        Serial.print(":");
        Serial.print(Triac1Delay);
        Serial.print(",");
        Serial.print(Triac2ID);
        Serial.print(":");
        Serial.print(Triac2Delay);
        Serial.print(",");
        Serial.print(Triac3ID);
        Serial.print(":");
        Serial.print(Triac3Delay);
        Serial.print(",");
        Serial.print(Triac4ID);
        Serial.print(":");
        Serial.print(Triac4Delay);
        Serial.print(",");
        Serial.print(ZTten1ID);
        Serial.print(":");
        Serial.print(ZTten1);
        Serial.print(",");
        Serial.print(ZTten2ID);
        Serial.print(":");
        Serial.print(ZTten2);
        Serial.print(",");
        Serial.print(ZTten3ID);
        Serial.print(":");
        Serial.print(ZTten3);
        Serial.print(",");
        Serial.print(ZTten4ID);
        Serial.print(":");
        Serial.println(ZTten4);
                          // clear the string:
        InputString = "";
        StringComplete = false;
        Comand = 0;
        Comand1 = 0;
        ActuatorValString = "";
        ActuatorValInt = 0;
        ActuatorValFloat = 0;
        for(int i = 0; i < 8; i++) {
        ActuatorValCharArray[i] = ' ';
        }
        break; 
        
      case 14: 
          //Write LED Brightness Value (0-100%)
        calcfunction();
        EEPROM.writeInt(192, ActuatorValInt);  //Save value to EEPROM
        LEDB = ActuatorValInt;
        Serial.print(ID);  //Group ID
        Serial.print("/");
        Serial.println("1"); 
                   // clear the string:
        InputString = "";
        StringComplete = false;
        Comand = 0;
        Comand1 = 0;
        ActuatorValString = "";
        ActuatorValInt = 0;
        ActuatorValFloat = 0;
        for(int i = 0; i < 8; i++) {
        ActuatorValCharArray[i] = ' ';
        }
        break; 

      case 17:
        //Enable/Disable Watchdog timer (1/0)
        calcfunction();
        EEPROM.writeInt(210, ActuatorValInt);  //Save value to EEPROM
        WatchDogEnable = ActuatorValInt;
        Serial.print(ID);  //Group ID
        Serial.print("/");
        Serial.println("1"); 
                 // clear the string:
        InputString = "";
        StringComplete = false;
        Comand = 0;
        Comand1 = 0;
        ActuatorValString = "";
        ActuatorValInt = 0;
        ActuatorValFloat = 0;
        for(int i = 0; i < 8; i++) {
        ActuatorValCharArray[i] = ' ';
        }
        break; 
        
     case 20: 
          //Write Zero to 10 #1 Fail Safe Value (0-100%)
        calcfunction();
        EEPROM.writeInt(194, ActuatorValInt);  //Save value to EEPROM
        ZTten1FS = ActuatorValInt;
        Serial.print(ID);  //Group ID
        Serial.print("/");
        Serial.println("1"); 
                   // clear the string:
        InputString = "";
        StringComplete = false;
        Comand = 0;
        Comand1 = 0;
        ActuatorValString = "";
        ActuatorValInt = 0;
        ActuatorValFloat = 0;
        for(int i = 0; i < 8; i++) {
        ActuatorValCharArray[i] = ' ';
        }
        break; 
     
     case 21: 
          //Write Zero to 10 #2 Fail Safe Value (0-100%)
        calcfunction();
        EEPROM.writeInt(196, ActuatorValInt);  //Save value to EEPROM
        ZTten2FS = ActuatorValInt;
        Serial.print(ID);  //Group ID
        Serial.print("/");
        Serial.println("1"); 
                   // clear the string:
        InputString = "";
        StringComplete = false;
        Comand = 0;
        Comand1 = 0;
        ActuatorValString = "";
        ActuatorValInt = 0;
        ActuatorValFloat = 0;
        for(int i = 0; i < 8; i++) {
        ActuatorValCharArray[i] = ' ';
        }
        break; 
        
      case 22: 
          //Write Zero to 10 #3 Fail Safe Value (0-100%)
        calcfunction();
        EEPROM.writeInt(198, ActuatorValInt);  //Save value to EEPROM
        ZTten3FS = ActuatorValInt;
        Serial.print(ID);  //Group ID
        Serial.print("/");
        Serial.println("1"); 
                   // clear the string:
        InputString = "";
        StringComplete = false;
        Comand = 0;
        Comand1 = 0;
        ActuatorValString = "";
        ActuatorValInt = 0;
        ActuatorValFloat = 0;
        for(int i = 0; i < 8; i++) {
        ActuatorValCharArray[i] = ' ';
        }
        break; 
        
      case 23: 
          //Write Zero to 10 #4 Fail Safe Value (0-100%)
        calcfunction();
        EEPROM.writeInt(200, ActuatorValInt);  //Save value to EEPROM
        ZTten4FS = ActuatorValInt;
        Serial.print(ID);  //Group ID
        Serial.print("/");
        Serial.println("1"); 
                   // clear the string:
        InputString = "";
        StringComplete = false;
        Comand = 0;
        Comand1 = 0;
        ActuatorValString = "";
        ActuatorValInt = 0;
        ActuatorValFloat = 0;
        for(int i = 0; i < 8; i++) {
        ActuatorValCharArray[i] = ' ';
        }
        break; 
     
     case 24: 
          //Write Triac #1 Fail Safe Value (0-100%)
        calcfunction();
        EEPROM.writeInt(194, ActuatorValInt);  //Save value to EEPROM
        Triac1FS = ActuatorValInt;
        Serial.print(ID);  //Group ID
        Serial.print("/");
        Serial.println("1"); 
                   // clear the string:
        InputString = "";
        StringComplete = false;
        Comand = 0;
        Comand1 = 0;
        ActuatorValString = "";
        ActuatorValInt = 0;
        ActuatorValFloat = 0;
        for(int i = 0; i < 8; i++) {
        ActuatorValCharArray[i] = ' ';
        }
        break; 
     
     case 25: 
          //Write Triac #2 Fail Safe Value (0-100%)
        calcfunction();
        EEPROM.writeInt(196, ActuatorValInt);  //Save value to EEPROM
        Triac2FS = ActuatorValInt;
        Serial.print(ID);  //Group ID
        Serial.print("/");
        Serial.println("1"); 
                   // clear the string:
        InputString = "";
        StringComplete = false;
        Comand = 0;
        Comand1 = 0;
        ActuatorValString = "";
        ActuatorValInt = 0;
        ActuatorValFloat = 0;
        for(int i = 0; i < 8; i++) {
        ActuatorValCharArray[i] = ' ';
        }
        break; 
        
      case 26: 
          //Write Triac #3 Fail Safe Value (0-100%)
        calcfunction();
        EEPROM.writeInt(198, ActuatorValInt);  //Save value to EEPROM
        Triac3FS = ActuatorValInt;
        Serial.print(ID);  //Group ID
        Serial.print("/");
        Serial.println("1"); 
                   // clear the string:
        InputString = "";
        StringComplete = false;
        Comand = 0;
        Comand1 = 0;
        ActuatorValString = "";
        ActuatorValInt = 0;
        ActuatorValFloat = 0;
        for(int i = 0; i < 8; i++) {
        ActuatorValCharArray[i] = ' ';
        }
        break; 
        
      case 27: 
          //Write Triac #4 Fail Safe Value (0-100%)
        calcfunction();
        EEPROM.writeInt(200, ActuatorValInt);  //Save value to EEPROM
        Triac4FS = ActuatorValInt;
        Serial.print(ID);  //Group ID
        Serial.print("/");
        Serial.println("1"); 
                   // clear the string:
        InputString = "";
        StringComplete = false;
        Comand = 0;
        Comand1 = 0;
        ActuatorValString = "";
        ActuatorValInt = 0;
        ActuatorValFloat = 0;
        for(int i = 0; i < 8; i++) {
        ActuatorValCharArray[i] = ' ';
        }
        break; 
        
      case 30:
          //Write Group ID to EEPROM
        calcfunction();
        EEPROM.writeBlock<char>(216, ActuatorValCharArray, 8);
        //Read ID
        EEPROM.readBlock<char>(216, ID, 8);
        Serial.print(ID);  //Group ID
        Serial.print("/");
        Serial.println("1"); 
//        Serial.println(ActuatorValCharArray);
                 // clear the string:
        InputString = "";
        StringComplete = false;
        Comand = 0;
        Comand1 = 0;
        ActuatorValString = "";
        ActuatorValInt = 0;
        ActuatorValFloat = 0;
        for(int i = 0; i < 8; i++) {
        ActuatorValCharArray[i] = ' ';
        }
        break; 
       
      case 31:  
        //Write 1st Element ID to EEPROM *PCBTempf*
        calcfunction();
        EEPROM.writeBlock<char>(8, ActuatorValCharArray, 8);
        //Read ID
        EEPROM.readBlock<char>(8, PCBTempfID, 8);
        Serial.print(ID);  //Group ID
        Serial.print("/");
        Serial.println("1"); 
                 // clear the string:
        InputString = "";
        StringComplete = false;
        Comand = 0;
        Comand1 = 0;
        ActuatorValString = "";
        ActuatorValInt = 0;
        ActuatorValFloat = 0;
        for(int i = 0; i < 8; i++) {
        ActuatorValCharArray[i] = ' ';
        }
        break; 
        
      case 32:  
        //Write 2nd Element ID to EEPROM *PCBTempc*
        calcfunction();
        EEPROM.writeBlock<char>(24,ActuatorValCharArray, 8);
        //Read ID
        EEPROM.readBlock<char>(24, PCBTempcID, 8);
        Serial.print(ID);  //Group ID
        Serial.print("/");
        Serial.println("1"); 
               // clear the string:
        InputString = "";
        StringComplete = false;
        Comand = 0;
        Comand1 = 0;
        ActuatorValString = "";
        ActuatorValInt = 0;
        ActuatorValFloat = 0;
        for(int i = 0; i < 8; i++) {
        ActuatorValCharArray[i] = ' ';
        }
        break; 
        
      case 33:  
        //Write 3rd Element ID to EEPROM *pH* 
        calcfunction();
        EEPROM.writeBlock<char>(40,ActuatorValCharArray, 8);
        //Read ID
        EEPROM.readBlock<char>(40, pHID, 8);
        Serial.print(ID);  //Group ID
        Serial.print("/");
        Serial.println("1"); 
                 // clear the string:
        InputString = "";
        StringComplete = false;
        Comand = 0;
        Comand1 = 0;
        ActuatorValString = "";
        ActuatorValInt = 0;
        ActuatorValFloat = 0;
        for(int i = 0; i < 8; i++) {
        ActuatorValCharArray[i] = ' ';
        }
        break; 
        
      case 34:  
        //Write 4th Element ID to EEPROM *DIY1*  
        calcfunction();
        EEPROM.writeBlock<char>(56,ActuatorValCharArray, 8);
        //Read ID
        EEPROM.readBlock<char>(56, DIY1ID, 8);
        Serial.print(ID);  //Group ID
        Serial.print("/");
        Serial.println("1"); 
                 // clear the string:
        InputString = "";
        StringComplete = false;
        Comand = 0;
        Comand1 = 0;
        ActuatorValString = "";
        ActuatorValInt = 0;
        ActuatorValFloat = 0;
        for(int i = 0; i < 8; i++) {
        ActuatorValCharArray[i] = ' ';
        }
        break; 
       
      case 35:  
        //Write 5th Element ID to EEPROM *DIY2*
        calcfunction();
        EEPROM.writeBlock<char>(72,ActuatorValCharArray, 8);
        //Read ID
        EEPROM.readBlock<char>(72, DIY2ID, 8);
        Serial.print(ID);  //Group ID
        Serial.print("/");
        Serial.println("1"); 
                 // clear the string:
        InputString = "";
        StringComplete = false;
        Comand = 0;
        Comand1 = 0;
        ActuatorValString = "";
        ActuatorValInt = 0;
        ActuatorValFloat = 0;
        for(int i = 0; i < 8; i++) {
        ActuatorValCharArray[i] = ' ';
        }
        break; 
       
      case 36:  
        //Write 6th Element ID to EEPROM *DIY3*
        calcfunction();
        EEPROM.writeBlock<char>(88,ActuatorValCharArray, 8);
        //Read ID
        EEPROM.readBlock<char>(88, DIY3ID, 8);
        Serial.print(ID);  //Group ID
        Serial.print("/");
        Serial.println("1"); 
                 // clear the string:
        InputString = "";
        StringComplete = false;
        Comand = 0;
        Comand1 = 0;
        ActuatorValString = "";
        ActuatorValInt = 0;
        ActuatorValFloat = 0;
        for(int i = 0; i < 8; i++) {
        ActuatorValCharArray[i] = ' ';
        }
        break; 
       
      case 37:  
        //Write 7th Element ID to EEPROM *DIY4* 
        calcfunction();
        EEPROM.writeBlock<char>(104,ActuatorValCharArray, 8);
        //Read ID
        EEPROM.readBlock<char>(104, DIY4ID, 8);
        Serial.print(ID);  //Group ID
        Serial.print("/");
        Serial.println("1"); 
                 // clear the string:
        InputString = "";
        StringComplete = false;
        Comand = 0;
        Comand1 = 0;
        ActuatorValString = "";
        ActuatorValInt = 0;
        ActuatorValFloat = 0;
        for(int i = 0; i < 8; i++) {
        ActuatorValCharArray[i] = ' ';
        }
        break; 
       
      case 38:
        //Write 8th Element ID (1st Actuator ID) to EEPROM *LEDB*  LED Brightness 
        calcfunction();
        EEPROM.writeBlock<char>(120,ActuatorValCharArray, 8);
        //Read ID
        EEPROM.readBlock<char>(120, LEDBID, 8);
        Serial.print(ID);  //Group ID
        Serial.print("/");
        Serial.println("1"); 
                 // clear the string:
        InputString = "";
        StringComplete = false;
        Comand = 0;
        Comand1 = 0;
        ActuatorValString = "";
        ActuatorValInt = 0;
        ActuatorValFloat = 0;
        for(int i = 0; i < 8; i++) {
        ActuatorValCharArray[i] = ' ';
        }
        break; 
        
      case 39:
         //Write 9th Element ID (2nd Actuator ID) to EEPROM *Zero to 10 #1*
        calcfunction();
        EEPROM.writeBlock<char>(128,ActuatorValCharArray, 8);
        //Read ID
        EEPROM.readBlock<char>(128, ZTten1ID, 8);
        Serial.print(ID);  //Group ID
        Serial.print("/");
        Serial.println("1"); 
                 // clear the string:
        InputString = "";
        StringComplete = false;
        Comand = 0;
        Comand1 = 0;
        ActuatorValString = "";
        ActuatorValInt = 0;
        ActuatorValFloat = 0;
        for(int i = 0; i < 8; i++) {
        ActuatorValCharArray[i] = ' ';
        }
        break; 
      
      case 40:
         //Write 10th Element ID (3rd Actuator ID) to EEPROM *Zero to 10 #2*
        calcfunction();
        EEPROM.writeBlock<char>(136,ActuatorValCharArray, 8);
        //Read ID
        EEPROM.readBlock<char>(136, ZTten2ID, 8);
        Serial.print(ID);  //Group ID
        Serial.print("/");
        Serial.println("1"); 
                 // clear the string:
        InputString = "";
        StringComplete = false;
        Comand = 0;
        Comand1 = 0;
        ActuatorValString = "";
        ActuatorValInt = 0;
        ActuatorValFloat = 0;
        for(int i = 0; i < 8; i++) {
        ActuatorValCharArray[i] = ' ';
        }
        break;  
      
      case 41:
         //Write 11th Element ID (4th Actuator ID) to EEPROM *Zero to 10 #3*
        calcfunction();
        EEPROM.writeBlock<char>(144,ActuatorValCharArray, 8);
        //Read ID
        EEPROM.readBlock<char>(144, ZTten3ID, 8);
        Serial.print(ID);  //Group ID
        Serial.print("/");
        Serial.println("1"); 
                 // clear the string:
        InputString = "";
        StringComplete = false;
        Comand = 0;
        Comand1 = 0;
        ActuatorValString = "";
        ActuatorValInt = 0;
        ActuatorValFloat = 0;
        for(int i = 0; i < 8; i++) {
        ActuatorValCharArray[i] = ' ';
        }
        break; 
      
     case 42:
         //Write 12th Element ID (5th Actuator ID) to EEPROM *Zero to 10 #4*
        calcfunction();
        EEPROM.writeBlock<char>(152,ActuatorValCharArray, 8);
        //Read ID
        EEPROM.readBlock<char>(152, ZTten4ID, 8);
        Serial.print(ID);  //Group ID
        Serial.print("/");
        Serial.println("1"); 
                 // clear the string:
        InputString = "";
        StringComplete = false;
        Comand = 0;
        Comand1 = 0;
        ActuatorValString = "";
        ActuatorValInt = 0;
        ActuatorValFloat = 0;
        for(int i = 0; i < 8; i++) {
        ActuatorValCharArray[i] = ' ';
        }
        break; 
      
      case 43:
         //Write 13th Element ID (6th Actuator) ID to EEPROM *115vac outlet #1*
        calcfunction();
        EEPROM.writeBlock<char>(160,ActuatorValCharArray, 8);
        //Read ID
        EEPROM.readBlock<char>(160, Triac1ID, 8);
        Serial.print(ID);  //Group ID
        Serial.print("/");
        Serial.println("1"); 
                 // clear the string:
        InputString = "";
        StringComplete = false;
        Comand = 0;
        Comand1 = 0;
        ActuatorValString = "";
        ActuatorValInt = 0;
        ActuatorValFloat = 0;
        for(int i = 0; i < 8; i++) {
        ActuatorValCharArray[i] = ' ';
        }
        break; 
      
     case 44:
         //Write 14th Element ID (7th Actuator ID) to EEPROM *115vac outlet #2*
        calcfunction();
        EEPROM.writeBlock<char>(168,ActuatorValCharArray, 8);
        //Read ID
        EEPROM.readBlock<char>(168, Triac2ID, 8);
        Serial.print(ID);  //Group ID
        Serial.print("/");
        Serial.println("1"); 
                 // clear the string:
        InputString = "";
        StringComplete = false;
        Comand = 0;
        Comand1 = 0;
        ActuatorValString = "";
        ActuatorValInt = 0;
        ActuatorValFloat = 0;
        for(int i = 0; i < 8; i++) {
        ActuatorValCharArray[i] = ' ';
        }
        break; 
      
      case 45:
         //Write 15th Element ID (8th Actuator ID) to EEPROM *115vac outlet #3*
        calcfunction();
        EEPROM.writeBlock<char>(176,ActuatorValCharArray, 8);
        //Read ID
        EEPROM.readBlock<char>(176, Triac3ID, 8);
        Serial.print(ID);  //Group ID
        Serial.print("/");
        Serial.println("1"); 
                 // clear the string:
        InputString = "";
        StringComplete = false;
        Comand = 0;
        Comand1 = 0;
        ActuatorValString = "";
        ActuatorValInt = 0;
        ActuatorValFloat = 0;
        for(int i = 0; i < 8; i++) {
        ActuatorValCharArray[i] = ' ';
        }
        break; 
      
      case 46:
         //Write 16th Element ID (9th Actuator ID) to EEPROM *115vac outlet #4*
        calcfunction();
        EEPROM.writeBlock<char>(184,ActuatorValCharArray, 8);
        //Read ID
        EEPROM.readBlock<char>(184, Triac4ID, 8);
        Serial.print(ID);  //Group ID
        Serial.print("/");
        Serial.println("1"); 
                 // clear the string:
        InputString = "";
        StringComplete = false;
        Comand = 0;
        Comand1 = 0;
        ActuatorValString = "";
        ActuatorValInt = 0;
        ActuatorValFloat = 0;
        for(int i = 0; i < 8; i++) {
        ActuatorValCharArray[i] = ' ';
        }
        break; 
      
      case 50:
        //Write 1st Element Calibration Slope (M) to EEPROM *PCBTempf* eep-16
        calcfunction();
        EEPROM.writeFloat(16, ActuatorValFloat);  //Save value to EEPROM
        PCBTempCalM = ActuatorValFloat;
        Serial.print(ID);  //Group ID
        Serial.print("/");
        Serial.println("1"); 
        
                 // clear the string:
        InputString = "";
        StringComplete = false;
        Comand = 0;
        Comand1 = 0;
        ActuatorValString = "";
        ActuatorValInt = 0;
        ActuatorValFloat = 0;
        for(int i = 0; i < 8; i++) {
        ActuatorValCharArray[i] = ' ';
        }
        break; 
        
      case 51:
        //Write 1st Element Calibration Y-inter (B) to EEPROM *PCBTempf* ee-20
        calcfunction();
        EEPROM.writeFloat(20, ActuatorValFloat);  //Save value to EEPROM
        PCBTempCalB = ActuatorValFloat;
        Serial.print(ID);  //Group ID
        Serial.print("/");
        Serial.println("1"); 
        
                 // clear the string:
        InputString = "";
        StringComplete = false;
        Comand = 0;
        Comand1 = 0;
        ActuatorValString = "";
        ActuatorValInt = 0;
        ActuatorValFloat = 0;
        for(int i = 0; i < 8; i++) {
        ActuatorValCharArray[i] = ' ';
        }
        break; 
      
      case 52:
        //Write 2nd Element Calibration Slope (M) to EEPROM ** ee-32
                 // clear the string:
        InputString = "";
        StringComplete = false;
        Comand = 0;
        Comand1 = 0;
        ActuatorValString = "";
        ActuatorValInt = 0;
        ActuatorValFloat = 0;
        for(int i = 0; i < 8; i++) {
        ActuatorValCharArray[i] = ' ';
        }
        break; 
      
      case 53:
        //Write 2nd Element Calibration Y-inter (B) to EEPROM ** ee-36
                 // clear the string:
        InputString = "";
        StringComplete = false;
        Comand = 0;
        Comand1 = 0;
        ActuatorValString = "";
        ActuatorValInt = 0;
        ActuatorValFloat = 0;
        for(int i = 0; i < 8; i++) {
        ActuatorValCharArray[i] = ' ';
        }
        break; 
        
      case 54:
        //Write 3rd Element Calibration Slope (M) to EEPROM *pH* eep-48
        calcfunction();
        EEPROM.writeFloat(48, ActuatorValFloat);  //Save value to EEPROM
        pHCalM = ActuatorValFloat;
        Serial.print(ID);  //Group ID
        Serial.print("/");
        Serial.println("1"); 
        
                 // clear the string:
        InputString = "";
        StringComplete = false;
        Comand = 0;
        Comand1 = 0;
        ActuatorValString = "";
        ActuatorValInt = 0;
        ActuatorValFloat = 0;
        for(int i = 0; i < 8; i++) {
        ActuatorValCharArray[i] = ' ';
        }
        break; 
        
      case 55:
        //Write 3rd Element Calibration Y-inter (B) to EEPROM *pH* ee-52
        calcfunction();
        EEPROM.writeFloat(52, ActuatorValFloat);  //Save value to EEPROM
        pHCalB = ActuatorValFloat;
        Serial.print(ID);  //Group ID
        Serial.print("/");
        Serial.println("1"); 
        
                 // clear the string:
        InputString = "";
        StringComplete = false;
        Comand = 0;
        Comand1 = 0;
        ActuatorValString = "";
        ActuatorValInt = 0;
        ActuatorValFloat = 0;
        for(int i = 0; i < 8; i++) {
        ActuatorValCharArray[i] = ' ';
        }
        break; 

      case 56:
        //Write 4th Element Calibration Slope (M) to EEPROM *DIY1* eep-64
        calcfunction();
        EEPROM.writeFloat(64, ActuatorValFloat);  //Save value to EEPROM
        DIY1CalM = ActuatorValFloat;
        Serial.print(ID);  //Group ID
        Serial.print("/");
        Serial.println("1"); 
        
                 // clear the string:
        InputString = "";
        StringComplete = false;
        Comand = 0;
        Comand1 = 0;
        ActuatorValString = "";
        ActuatorValInt = 0;
        ActuatorValFloat = 0;
        for(int i = 0; i < 8; i++) {
        ActuatorValCharArray[i] = ' ';
        }
        break; 
        
      case 57:
        //Write 4th Element Calibration Y-inter (B) to EEPROM *DIY1* ee-68
        calcfunction();
        EEPROM.writeFloat(68, ActuatorValFloat);  //Save value to EEPROM
        DIY1CalB = ActuatorValFloat;
        Serial.print(ID);  //Group ID
        Serial.print("/");
        Serial.println("1"); 
        
                 // clear the string:
        InputString = "";
        StringComplete = false;
        Comand = 0;
        Comand1 = 0;
        ActuatorValString = "";
        ActuatorValInt = 0;
        ActuatorValFloat = 0;
        for(int i = 0; i < 8; i++) {
        ActuatorValCharArray[i] = ' ';
        }
        break; 

      case 58:
        //Write 5th Element Calibration Slope (M) to EEPROM *DIY2* eep-80
        calcfunction();
        EEPROM.writeFloat(80, ActuatorValFloat);  //Save value to EEPROM
        DIY2CalM = ActuatorValFloat;
        Serial.print(ID);  //Group ID
        Serial.print("/");
        Serial.println("1"); 
        
                 // clear the string:
        InputString = "";
        StringComplete = false;
        Comand = 0;
        Comand1 = 0;
        ActuatorValString = "";
        ActuatorValInt = 0;
        ActuatorValFloat = 0;
        for(int i = 0; i < 8; i++) {
        ActuatorValCharArray[i] = ' ';
        }
        break; 
        
      case 59:
        //Write 5th Element Calibration Y-inter (B) to EEPROM *DIY2* ee-84
        calcfunction();
        EEPROM.writeFloat(84, ActuatorValFloat);  //Save value to EEPROM
        DIY2CalB = ActuatorValFloat;
        Serial.print(ID);  //Group ID
        Serial.print("/");
        Serial.println("1"); 
        
                 // clear the string:
        InputString = "";
        StringComplete = false;
        Comand = 0;
        Comand1 = 0;
        ActuatorValString = "";
        ActuatorValInt = 0;
        ActuatorValFloat = 0;
        for(int i = 0; i < 8; i++) {
        ActuatorValCharArray[i] = ' ';
        }
        break; 

      case 60:
        //Write 6th Element Calibration Slope (M) to EEPROM *DIY3* eep-96
        calcfunction();
        EEPROM.writeFloat(96, ActuatorValFloat);  //Save value to EEPROM
        DIY3CalM = ActuatorValFloat;
        Serial.print(ID);  //Group ID
        Serial.print("/");
        Serial.println("1"); 
        
                 // clear the string:
        InputString = "";
        StringComplete = false;
        Comand = 0;
        Comand1 = 0;
        ActuatorValString = "";
        ActuatorValInt = 0;
        ActuatorValFloat = 0;
        for(int i = 0; i < 8; i++) {
        ActuatorValCharArray[i] = ' ';
        }
        break; 
        
      case 61:
        //Write 6th Element Calibration Y-inter (B) to EEPROM *DIY3* ee-100
        calcfunction();
        EEPROM.writeFloat(100, ActuatorValFloat);  //Save value to EEPROM
        DIY3CalB = ActuatorValFloat;
        Serial.print(ID);  //Group ID
        Serial.print("/");
        Serial.println("1"); 
        
                 // clear the string:
        InputString = "";
        StringComplete = false;
        Comand = 0;
        Comand1 = 0;
        ActuatorValString = "";
        ActuatorValInt = 0;
        ActuatorValFloat = 0;
        for(int i = 0; i < 8; i++) {
        ActuatorValCharArray[i] = ' ';
        }
        break; 

      case 62:
        //Write 7th Element Calibration Slope (M) to EEPROM *DIY4* eep-112
        calcfunction();
        EEPROM.writeFloat(112, ActuatorValFloat);  //Save value to EEPROM
        DIY4CalM = ActuatorValFloat;
        Serial.print(ID);  //Group ID
        Serial.print("/");
        Serial.println("1"); 
                 // clear the string:
        InputString = "";
        StringComplete = false;
        Comand = 0;
        Comand1 = 0;
        ActuatorValString = "";
        ActuatorValInt = 0;
        ActuatorValFloat = 0;
        for(int i = 0; i < 8; i++) {
        ActuatorValCharArray[i] = ' ';
        }
        break; 
        
      case 63:
        //Write 7th Element Calibration Y-inter (B) to EEPROM *DIY4* ee-116
        calcfunction();
        EEPROM.writeFloat(116, ActuatorValFloat);  //Save value to EEPROM
        DIY4CalB = ActuatorValFloat;
        Serial.print(ID);  //Group ID
        Serial.print("/");
        Serial.println("1"); 
                 // clear the string:
        InputString = "";
        StringComplete = false;
        Comand = 0;
        Comand1 = 0;
        ActuatorValString = "";
        ActuatorValInt = 0;
        ActuatorValFloat = 0;
        for(int i = 0; i < 8; i++) {
        ActuatorValCharArray[i] = ' ';
        }
        break; 



      default:
        Serial.print(ID);  //Group ID
        Serial.print("/");
        Serial.println("0"); 
                 // clear the string:
        InputString = "";
        StringComplete = false;
        Comand = 0;
        Comand1 = 0;
        ActuatorValString = "";
        ActuatorValInt = 0;
        ActuatorValFloat = 0;
        for(int i = 0; i < 8; i++) {
        ActuatorValCharArray[i] = ' ';
        }
        break;   
      } 
    } 
}


//Deals with incoming Serial Messages
void serialEvent() {
  String IDIn = "";
  while (Serial.available()) {
    char inChar = (char)Serial.read();   // get the new byte:
  //  Serial.print("inChar: ");
  //  Serial.println(inChar);  
    InputString += inChar;    // add it to the inputString:
 //   Serial.print("InputString: ");
 //   Serial.println(InputString);  
    if (inChar == '!') {     // if the incoming character is a newline, set a flag
      for (int i = 0; i<= 7; i++) {  //Read the ID
        char IDRead = InputString.charAt(i);
        IDIn += IDRead;
        
      }
      if (IDIn == ID) {   //If the IDs (IDs) Match
        if (SendData) {
          Pause = true;  //if it's sending continuous data, stop for a bit
        }
        StringComplete = true;    //Tell the main program there is input
        IDIn = "";
      } 
      else if ((InputString.charAt(0) == '0') && (InputString.length() == 2)) {
        //Serial.print("ID is: ");
        Serial.print(ID);  //Print out ID
        Serial.print("/");
        Serial.println(ID);
        IDIn = "";
        StringComplete = false;
        InputString = "";
      }
      else {
        Serial.print(ID);  //Group ID
        Serial.print("/");
        Serial.println("-1"); 
      //  Serial.println("ID's dont match");
       // Serial.println(IDIn);
        IDIn = "";
        StringComplete = false;
        InputString = "";
      }
    }  
  }  
}


//Processes incoming string
void calcfunction() {
  String ActuatorValString = "";
  int StringLength = InputString.length();
  int x1 = InputString.charAt(9) - '0'; //converting char 9 (command #) to int
  
  if ((x1 >= 0) && (x1 <=9)) {  //If it is a 2 digit request
    for (int i = 11; i < StringLength; i++) {
      char ActuatorRead = InputString.charAt(i);
      ActuatorValString += ActuatorRead;
    }
  }
  else {   //If it is a 1 digit request
    for (int i = 10; i < StringLength; i++) {
      char ActuatorRead = InputString.charAt(i);
      ActuatorValString += ActuatorRead;
    }
  }
  
  ActuatorValString.toCharArray(ActuatorValCharArray, ActuatorValString.length());    //convert to Char array for EEPROM
  ActuatorValInt = ActuatorValString.toInt();  //An integer of the serial input
  ActuatorValFloat = ActuatorValString.toFloat();  //A float of the serial input
  //Serial.println(ActuatorValInt);
}


