/*  Generic sketch for an Ai Arduino
 * 
 *
 * Copyright 2015 Scott Tomko, Greg Tomko, Linda Close
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * (GNU General Public License for more details.

 * You should have received a copy of the GNU Lesser General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * Contact:  Staff@ReefAi.com
 *
 *
Arduino Pinout:
2
3
4
5
6
7
8
9
10
11 (mosi)
12 (miso)
13 (sck)
14
15
16
17
18 SDA
19 SCL
A6 (20)
A7 (21)
*/
/* Ryan Hub Element List
Element, Function, Pin#
1  , Temp #1  , 20
2  , Temp #2  , 21
3 ,  Temp #3  , 14
4 ,  Temp #4  , 19
5 ,  Temp #5  , 18
6 ,  Temp #6  , 17
7 ,  Temp #7  , 16
8 ,  Temp #8  , 15
9 ,  Float #1 , 4
10 , Float #2 , 2
11 , Float #3 , 7
12 , Float #4 , 8
13 , Float #5 , 13
14 , Float #6 , 12
15 , PWM #1   , 11
16 , PWM #2   , 10
17 , PWM #3   , 9
18 , PWM #4   , 6
19 , PWM #5   , 5
20 , PWM #6   , 3
*/


#include <Ocom.h>  //include the ocom library
#include <EEPROMex.h>  //This must be included for Ocom to work
Ocom ocom;  //   Don't put () if it isn't getting a variable


//*******************************Set These************************************
  const int NumberOfElements = 20;
  const int ElementPins[NumberOfElements+1] = {20,21,14,19,18,17,16,15,4,2,7,8,13,12,11,10,9,6,5,3};//  <--SET THE ARDUINO ELEMENT PINS HERE
    //Pre-set these here, but eeprom will over-write them (The EEPROM Loader program sets them as 'z' & '10', which this program ignors)
  const char ElementTypes[NumberOfElements+1] = {'s','s','s','s','s','s','s','s','s','s','s','s','s','s','a','a','a','a','a','a'};     //s=sensor, a=actuator, n=nothing, 
                                                                                                               //z=EEPLoaded
  const int ElementFunctions[NumberOfElements+1] = {0,0,0,1,1,1,1,1,2,2,2,2,2,2,3,3,3,3,3,3};  // 0=unused, 1=analogRead, 2=digitalRead,
                                                            //          3=analogWrite, 4=digitalWrite, 5=triac, 6=ACPhase, 10=EEPLoaded
  int SensorReadDelayMS = 1000;   //Set how often to read sensors here (1000 = 1/sec, 500 = 2/sec)
//******************************************************************************


void setup() {  // put your setup code here, to run once:
    //ocom.Setup loads all neccessary data from eeprom, ect, and activates the USB connection
  ocom.Setup(ElementPins, ElementTypes, ElementFunctions, NumberOfElements);
}

void loop() {
    // put your main code here, to run repeatedly:
  ocom.Run(SensorReadDelayMS);  //Activates the ocom library every time the loop runs
}


